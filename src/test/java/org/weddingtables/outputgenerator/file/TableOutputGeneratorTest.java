package org.weddingtables.outputgenerator.file;

import org.junit.Test;
import org.weddingtables.exceptions.AlreadyOccupiedSeatException;
import org.weddingtables.exceptions.InvalidDataException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.RectangleTable;
import org.weddingtables.model.table.RoundTable;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Paweł on 2017-07-05.
 */
public class TableOutputGeneratorTest {

    @Test
    public void generateOutputForTableWithAloneGuestUnassignedToSeatsTest() throws Exception {
        //given
        TableOutputGenerator generator = new TableOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        Integer seats = 4;
        Integer guests = 3;
        ITable table = new RectangleTable(seats);

        //when
        for (Integer i = 1; i <= guests; i++) {
            table.addGuest(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        String expected = "{\n" +
                            "\"type\": \"RECTANGLE\",\n" +
                            "\"seats\": 4,\n" +
                            "\"guests\": [{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname1\",\n" +
                                "\"lastname\": \"lastname1\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname2\",\n" +
                                "\"lastname\": \"lastname2\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname3\",\n" +
                                "\"lastname\": \"lastname3\"\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(table));
    }

    @Test
    public void generateOutputForTableWithGuestInGroupsAssignedToSeatsTest() throws Exception {
        //given
        TableOutputGenerator generator = new TableOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        Integer guests = 3;
        ITable table = new RoundTable(guests + 2);
        ArrayList<GuestUnit> members = new ArrayList<>();

        //when
        Guest guest = new Guest(firstname, lastname);
        table.addGuest(guest);
        table.placeGuest(guest, 0);

        for (Integer i = 1; i < guests; i++) {
            members.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        GuestGroup group = new GuestGroup(members);
        table.addGuest(group);

        for (Integer i = 1; i < guests; i++) {
            table.placeGuest(group.getGuests().get(i-1), i);
        }

        String expected = "{\n" +
                            "\"type\": \"ROUND\",\n" +
                            "\"seats\": 5,\n" +
                            "\"guests\": [{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname\",\n" +
                                "\"lastname\": \"lastname\",\n" +
                                "\"place\": 0\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"GROUP\",\n" +
                                "\"members\": [{\n" +
                                    "\"type\": \"INDIVIDUAL\",\n" +
                                    "\"firstname\": \"firstname1\",\n" +
                                    "\"lastname\": \"lastname1\",\n" +
                                    "\"place\": 1\n" +
                                  "},\n" +
                                  "{\n" +
                                    "\"type\": \"INDIVIDUAL\",\n" +
                                    "\"firstname\": \"firstname2\",\n" +
                                    "\"lastname\": \"lastname2\",\n" +
                                    "\"place\": 2\n" +
                                  "}\n" +
                                "]\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(table));
    }
}
