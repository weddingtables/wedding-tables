package org.weddingtables.outputgenerator.file;

import org.junit.Test;
import org.weddingtables.model.ballroom.BallRoom;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.RectangleTable;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-06.
 */
public class BallRoomOutputGeneratorTest {
    @Test
    public void generateOutputForBallRoomWithGuestsAssignedToTableButUnassignedToSeatsTest() throws Exception {
        //given
        BallRoomOutputGenerator generator = new BallRoomOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        Integer newlywedsTableSeats = 2;
        Integer guestsTableSeats = 4;
        Integer guestNumber = 3;
        ITable newlywedsTable = new RectangleTable(newlywedsTableSeats);
        ITable guestsTable = new RectangleTable(guestsTableSeats);
        ArrayList<GuestUnit> newlyweds = new ArrayList<>();
        ArrayList<GuestUnit> guests = new ArrayList<>();

        //when
        newlyweds.add(new Guest("Bride", "JustMarried"));
        newlyweds.add(new Guest("Groom", "JustMarried"));
        GuestGroup newlywedsGroup = new GuestGroup(newlyweds);
        newlywedsTable.addGuest(newlywedsGroup);
        guests.addAll(newlyweds);

        for (Integer i = 1; i <= guestNumber; i++) {
            Guest guest = new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i));
            guestsTable.addGuest(guest);
            guests.add(guest);
        }

        BallRoom ballRoom = new BallRoom(newlywedsTable, new ITable[]{guestsTable}, guests);

        String expected = "{\n" +
                            "\"newlywedsTable\": {\n" +
                              "\"type\": \"RECTANGLE\",\n" +
                              "\"seats\": 2,\n" +
                              "\"guests\": [{\n" +
                                  "\"type\": \"GROUP\",\n" +
                                  "\"members\": [{\n" +
                                      "\"type\": \"INDIVIDUAL\",\n" +
                                      "\"firstname\": \"Bride\",\n" +
                                      "\"lastname\": \"JustMarried\"\n" +
                                    "},\n" +
                                    "{\n" +
                                      "\"type\": \"INDIVIDUAL\",\n" +
                                      "\"firstname\": \"Groom\",\n" +
                                      "\"lastname\": \"JustMarried\"\n" +
                                    "}\n" +
                                  "]\n" +
                                "}\n" +
                              "]\n" +
                            "},\n" +
                            "\"guestsTables\": [{\n" +
                                "\"type\": \"RECTANGLE\",\n" +
                                "\"seats\": 4,\n" +
                                "\"guests\": [{\n" +
                                    "\"type\": \"INDIVIDUAL\",\n" +
                                    "\"firstname\": \"firstname1\",\n" +
                                    "\"lastname\": \"lastname1\"\n" +
                                  "},\n" +
                                  "{\n" +
                                    "\"type\": \"INDIVIDUAL\",\n" +
                                    "\"firstname\": \"firstname2\",\n" +
                                    "\"lastname\": \"lastname2\"\n" +
                                  "},\n" +
                                  "{\n" +
                                    "\"type\": \"INDIVIDUAL\",\n" +
                                    "\"firstname\": \"firstname3\",\n" +
                                    "\"lastname\": \"lastname3\"\n" +
                                  "}\n" +
                                "]\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(ballRoom));
    }

    @Test
    public void generateOutputForBallRoomWithGuestsUnassignedToTablesTest() throws Exception {
        //given
        BallRoomOutputGenerator generator = new BallRoomOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        Integer newlywedsTableSeats = 2;
        Integer guestsTableSeats = 4;
        Integer guestNumber = 3;
        ITable newlywedsTable = new RectangleTable(newlywedsTableSeats);
        ITable guestsTable = new RectangleTable(guestsTableSeats);
        ArrayList<GuestUnit> newlyweds = new ArrayList<>();
        ArrayList<GuestUnit> guests = new ArrayList<>();

        //when
        newlyweds.add(new Guest("Bride", "JustMarried"));
        newlyweds.add(new Guest("Groom", "JustMarried"));
        GuestGroup newlywedsGroup = new GuestGroup(newlyweds);
        newlywedsTable.addGuest(newlywedsGroup);
        guests.addAll(newlyweds);

        for (Integer i = 1; i <= guestNumber; i++) {
            Guest guest = new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i));
            guests.add(guest);
        }

        BallRoom ballRoom = new BallRoom(newlywedsTable, new ITable[]{guestsTable}, guests);

        String expected = "{\n" +
                            "\"newlywedsTable\": {\n" +
                              "\"type\": \"RECTANGLE\",\n" +
                              "\"seats\": 2,\n" +
                              "\"guests\": [{\n" +
                                  "\"type\": \"GROUP\",\n" +
                                  "\"members\": [{\n" +
                                      "\"type\": \"INDIVIDUAL\",\n" +
                                      "\"firstname\": \"Bride\",\n" +
                                      "\"lastname\": \"JustMarried\"\n" +
                                    "},\n" +
                                    "{\n" +
                                      "\"type\": \"INDIVIDUAL\",\n" +
                                      "\"firstname\": \"Groom\",\n" +
                                      "\"lastname\": \"JustMarried\"\n" +
                                    "}\n" +
                                  "]\n" +
                                "}\n" +
                              "]\n" +
                            "},\n" +
                            "\"guestsTables\": [{\n" +
                                "\"type\": \"RECTANGLE\",\n" +
                                "\"seats\": 4,\n" +
                                "\"guests\": [\n" +
                                "]\n" +
                              "}\n" +
                            "],\n" +
                            "\"unplacedGuests\": [{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname1\",\n" +
                                "\"lastname\": \"lastname1\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname2\",\n" +
                                "\"lastname\": \"lastname2\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname3\",\n" +
                                "\"lastname\": \"lastname3\"\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(ballRoom));
    }
}
