package org.weddingtables.outputgenerator.file;

import org.junit.Test;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.RectangleTable;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-10.
 */
public class GuestUnitOutputGeneratorTest {

    @Test
    public void generateOutputForGuestWithoutAssignedTableTest() throws Exception {
        //given
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";

        //when
        Guest guest = new Guest(firstname, lastname);
        String expected = "{\n" +
                            "\"type\": \"INDIVIDUAL\",\n" +
                            "\"firstname\": \"firstname\",\n" +
                            "\"lastname\": \"lastname\"\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(guest));
    }

    @Test
    public void generateOutputForGuestWithTableButWithoutAssignedSeatTest() throws Exception {
        //given
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        ITable table = new RectangleTable(2);

        //when
        Guest guest = new Guest(firstname, lastname);
        table.addGuest(guest);
        String expected = "{\n" +
                            "\"type\": \"INDIVIDUAL\",\n" +
                            "\"firstname\": \"firstname\",\n" +
                            "\"lastname\": \"lastname\"\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(guest));
    }

    @Test
    public void generateOutputForGuestWithAssignedSeatTest() throws Exception {
        //given
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        ITable table = new RectangleTable(2);

        //when
        Guest guest = new Guest(firstname, lastname);
        table.addGuest(guest);
        table.placeGuest(guest,0);
        String expected = "{\n" +
                            "\"type\": \"INDIVIDUAL\",\n" +
                            "\"firstname\": \"firstname\",\n" +
                            "\"lastname\": \"lastname\",\n" +
                            "\"place\": 0\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(guest));
    }

    @Test
    public void generateOutputForGuestGroupWithoutAssignedTableTest() throws Exception {
        //given
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";

        //when
        ArrayList<GuestUnit> members = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            members.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        GuestUnit guestGroup = new GuestGroup(members);

        String expected = "{\n" +
                            "\"type\": \"GROUP\",\n" +
                            "\"members\": [{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname1\",\n" +
                                "\"lastname\": \"lastname1\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname2\",\n" +
                                "\"lastname\": \"lastname2\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname3\",\n" +
                                "\"lastname\": \"lastname3\"\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(guestGroup));
    }

    @Test
    public void generateOutputForGuestGroupWithTableButWithoutAssignedSeatTest() throws Exception {
        //given
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        ITable table = new RectangleTable(4);

        //when
        ArrayList<GuestUnit> members = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            members.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        GuestUnit guestGroup = new GuestGroup(members);
        table.addGuest(guestGroup);

        String expected = "{\n" +
                            "\"type\": \"GROUP\",\n" +
                            "\"members\": [{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname1\",\n" +
                                "\"lastname\": \"lastname1\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname2\",\n" +
                                "\"lastname\": \"lastname2\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname3\",\n" +
                                "\"lastname\": \"lastname3\"\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(guestGroup));
    }

    @Test
    public void generateOutputForGuestGroupWithTableAndOnlySomeOfMembersGotAssignedSeatTest() throws Exception {
        //given
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        ITable table = new RectangleTable(4);

        //when
        ArrayList<GuestUnit> members = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            members.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        GuestUnit guestGroup = new GuestGroup(members);
        table.addGuest(guestGroup);
        table.placeGuest((Guest) members.get(0),0);

        String expected = "{\n" +
                            "\"type\": \"GROUP\",\n" +
                            "\"members\": [{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname1\",\n" +
                                "\"lastname\": \"lastname1\",\n" +
                                "\"place\": 0\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname2\",\n" +
                                "\"lastname\": \"lastname2\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname3\",\n" +
                                "\"lastname\": \"lastname3\"\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(guestGroup));
    }

    @Test
    public void generateOutputForGuestGroupWereAllMembersGotAssignedSeatsTest() throws Exception {
        //given
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        ITable table = new RectangleTable(4);

        //when
        ArrayList<GuestUnit> members = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            members.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        GuestUnit guestGroup = new GuestGroup(members);
        table.addGuest(guestGroup);
        for (int i = 0; i < members.size(); i++) {
            table.placeGuest((Guest)members.get(i), i);
        }

        String expected = "{\n" +
                            "\"type\": \"GROUP\",\n" +
                            "\"members\": [{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname1\",\n" +
                                "\"lastname\": \"lastname1\",\n" +
                                "\"place\": 0\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname2\",\n" +
                                "\"lastname\": \"lastname2\",\n" +
                                "\"place\": 1\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname3\",\n" +
                                "\"lastname\": \"lastname3\",\n" +
                                "\"place\": 2\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(guestGroup));
    }

    @Test
    public void generateOutputForGuestGroupInGuestGroupTest() throws Exception {
        //given
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";

        //when
        ArrayList<GuestUnit> members1 = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            members1.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        GuestUnit guestGroup1 = new GuestGroup(members1);

        ArrayList<GuestUnit> members2 = new ArrayList<>();
        members2.add(new Guest(firstname + String.valueOf(3), lastname + String.valueOf(3)));
        members2.add(guestGroup1);
        GuestUnit guestGroup2 = new GuestGroup(members2);

        String expected = "{\n" +
                            "\"type\": \"GROUP\",\n" +
                            "\"members\": [{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname3\",\n" +
                                "\"lastname\": \"lastname3\"\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"GROUP\",\n" +
                                "\"members\": [{\n" +
                                    "\"type\": \"INDIVIDUAL\",\n" +
                                    "\"firstname\": \"firstname1\",\n" +
                                    "\"lastname\": \"lastname1\"\n" +
                                  "},\n" +
                                  "{\n" +
                                    "\"type\": \"INDIVIDUAL\",\n" +
                                    "\"firstname\": \"firstname2\",\n" +
                                    "\"lastname\": \"lastname2\"\n" +
                                  "}\n" +
                                "]\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(guestGroup2));
    }

    @Test
    public void generateOutputForGuestGroupInGuestGroupWithAllLocatedOnSeatsTest() throws Exception {
        //given
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        String firstname = "firstname";
        String lastname = "lastname";
        ITable table = new RectangleTable(4);

        //when
        ArrayList<GuestUnit> members1 = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            members1.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        GuestUnit guestGroup1 = new GuestGroup(members1);

        ArrayList<GuestUnit> members2 = new ArrayList<>();
        members2.add(new Guest(firstname + String.valueOf(3), lastname + String.valueOf(3)));
        members2.add(guestGroup1);
        GuestUnit guestGroup2 = new GuestGroup(members2);
        table.addGuest(guestGroup2);

        int i = 0;
        for (Guest guest : guestGroup2.getGuests()) {
            table.placeGuest(guest, i++);
        }

        String expected = "{\n" +
                            "\"type\": \"GROUP\",\n" +
                            "\"members\": [{\n" +
                                "\"type\": \"INDIVIDUAL\",\n" +
                                "\"firstname\": \"firstname3\",\n" +
                                "\"lastname\": \"lastname3\",\n" +
                                "\"place\": 0\n" +
                              "},\n" +
                              "{\n" +
                                "\"type\": \"GROUP\",\n" +
                                "\"members\": [{\n" +
                                    "\"type\": \"INDIVIDUAL\",\n" +
                                    "\"firstname\": \"firstname1\",\n" +
                                    "\"lastname\": \"lastname1\",\n" +
                                    "\"place\": 1\n" +
                                  "},\n" +
                                  "{\n" +
                                    "\"type\": \"INDIVIDUAL\",\n" +
                                    "\"firstname\": \"firstname2\",\n" +
                                    "\"lastname\": \"lastname2\",\n" +
                                    "\"place\": 2\n" +
                                  "}\n" +
                                "]\n" +
                              "}\n" +
                            "]\n" +
                          "}";

        //then
        assertEquals(expected, generator.generate(guestGroup2));
    }
}
