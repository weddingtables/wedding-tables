package org.weddingtables.outputgenerator;

import org.junit.Test;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-10.
 */
public class GuestOutputGeneratorTest {

    @Test
    public void generateOutputOnlyWithGuestsTest() throws Exception {
        //given
        GuestOutputGenerator generator = new GuestOutputGenerator();
        ArrayList<GuestUnit> guests = new ArrayList<>();
        Integer guestNumber = 4;
        String firstname = "firstname";
        String lastname = "lastname";

        //when
        for (Integer i = 1; i <= guestNumber; i++) {
            guests.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }

        String expected = "firstname1 lastname1\n" +
                          "firstname2 lastname2\n" +
                          "firstname3 lastname3\n" +
                          "firstname4 lastname4\n";

        //then
        assertEquals(expected, generator.generate(guests, ""));
    }

    @Test
    public void generateOutputWithGuestGroupTest() throws Exception {
        //given
        GuestOutputGenerator generator = new GuestOutputGenerator();
        ArrayList<GuestUnit> guests = new ArrayList<>();
        Integer membersNumber = 4;
        String firstname = "firstname";
        String lastname = "lastname";

        //when
        ArrayList<GuestUnit> members = new ArrayList<>();
        for (Integer i = 1; i <= membersNumber; i++) {
            members.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        guests.add(new Guest(firstname, lastname));
        guests.add(new GuestGroup(members));

        String expected = "firstname lastname\n" +
                "{\n" +
                "\tfirstname1 lastname1\n" +
                "\tfirstname2 lastname2\n" +
                "\tfirstname3 lastname3\n" +
                "\tfirstname4 lastname4\n" +
                "}\n";

        //then
        assertEquals(expected, generator.generate(guests, ""));
    }

    @Test
    public void generateOutputWithNestedGuestGroupsTest() throws Exception {
        //given
        GuestOutputGenerator generator = new GuestOutputGenerator();
        ArrayList<GuestUnit> guests = new ArrayList<>();
        Integer membersNumber = 4;
        String firstname = "firstname";
        String lastname = "lastname";

        //when
        ArrayList<GuestUnit> members = new ArrayList<>();
        ArrayList<GuestUnit> nestedMembers = new ArrayList<>();
        for (Integer i = 1; i <= membersNumber; i++) {
            nestedMembers.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }
        members.add(new Guest(firstname, lastname));
        members.add(new GuestGroup(nestedMembers));
        guests.add(new GuestGroup(members));

        String expected = "{\n" +
                          "\tfirstname lastname\n" +
                          "\t{\n" +
                          "\t\tfirstname1 lastname1\n" +
                          "\t\tfirstname2 lastname2\n" +
                          "\t\tfirstname3 lastname3\n" +
                          "\t\tfirstname4 lastname4\n" +
                          "\t}\n" +
                          "}\n";

        //then
        assertEquals(expected, generator.generate(guests, ""));
    }
}
