package org.weddingtables.model.guest;

import org.junit.Test;
import org.weddingtables.exceptions.IncompleteDataException;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-02.
 */
public class GuestUnitBuilderTest {
    @Test
    public void buildIndividualTest() throws Exception {
        //given
        GuestUnitBuilder builder = new GuestUnitBuilder(GuestUnitType.INDIVIDUAL);
        String firstname = "firstname";
        String lastname = "lastname";

        //when
        GuestUnit expected = new Guest(firstname, lastname);
        GuestUnit actual = builder.setFirstname(firstname)
                                  .setLastname(lastname)
                                  .build();

        //then
        assertEquals(expected, actual);
    }

    @Test(expected = IncompleteDataException.class)
    public void buildIndividualWithoutFirstnameTest() throws Exception {
        //given
        GuestUnitBuilder builder = new GuestUnitBuilder(GuestUnitType.INDIVIDUAL);
        String lastname = "lastname";

        //when
        GuestUnit actual = builder.setLastname(lastname)
                                  .build();
    }

    @Test(expected = IncompleteDataException.class)
    public void buildIndividualWithoutLastnameTest() throws Exception {
        //given
        GuestUnitBuilder builder = new GuestUnitBuilder(GuestUnitType.INDIVIDUAL);
        String firstname = "firstname";

        //when
        GuestUnit actual = builder.setFirstname(firstname)
                                  .build();
    }

    @Test
    public void buildGroupTest() throws Exception {
        //given
        GuestUnitBuilder builder = new GuestUnitBuilder(GuestUnitType.GROUP);
        GuestUnit guest1 = new Guest("firstname1", "lastname1");
        GuestUnit guest2 = new Guest("firstname2", "lastname2");
        GuestUnit guest3 = new Guest("firstname3", "lastname3");
        ArrayList<GuestUnit> members = new ArrayList<>();
        members.add(guest1);
        members.add(guest2);
        members.add(guest3);

        //when
        GuestUnit expected = new GuestGroup(members);
        GuestUnit actual = builder.addMember(guest1)
                                  .addMember(guest2)
                                  .addMember(guest3)
                                  .build();

        //then
        assertEquals(expected, actual);
    }

    @Test(expected = IncompleteDataException.class)
    public void buildGroupWithoutMembersTest() throws Exception {
        //given
        GuestUnitBuilder builder = new GuestUnitBuilder(GuestUnitType.GROUP);

        //when
        GuestUnit actual = builder.build();
    }
}
