package org.weddingtables.model.guest;

import org.junit.Test;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.RectangleTable;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-05.
 */
public class GuestGroupTest {
    @Test
    public void multiGuestGroupGetGuestTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";

        //when
        ArrayList<Guest> expected = new ArrayList<>();
        ArrayList<GuestUnit> members = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            Guest guest = new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i));
            members.add(guest);
            expected.add(guest);
        }
        GuestUnit guestGroup = new GuestGroup(members);

        //then
        assertEquals(expected, guestGroup.getGuests());
    }

    @Test
    public void nestedGroupsGetGuestTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";

        //when
        ArrayList<Guest> expected = new ArrayList<>();
        ArrayList<GuestUnit> members = new ArrayList<>();
        ArrayList<GuestUnit> members1 = new ArrayList<>();
        ArrayList<GuestUnit> members2 = new ArrayList<>();

        for (int i = 1; i <= 3; i++) {
            Guest guest = new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i));
            members1.add(guest);
            expected.add(guest);
        }
        GuestUnit guestGroup1 = new GuestGroup(members1);

        members.add(guestGroup1);
        Guest alone = new Guest(firstname, lastname);
        expected.add(alone);
        members.add(alone);

        for (int i = 1; i <= 3; i++) {
            Guest guest = new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i));
            members2.add(guest);
            expected.add(guest);
        }
        GuestUnit guestGroup2 = new GuestGroup(members2);
        members.add(guestGroup2);

        GuestUnit guestGroup = new GuestGroup(members);

        //then
        assertEquals(expected, guestGroup.getGuests());
    }
}
