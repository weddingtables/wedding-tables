package org.weddingtables.model.table;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-02.
 */
public class TableFactoryTest {
    private TableFactory factory;

    @Before
    public void setUp() {
        this.factory = new TableFactory();
    }

    @Test
    public void createRoundTableTest() throws Exception {
        //given
        Integer seats = 10;
        TableType type = TableType.ROUND;

        //when
        ITable expected = new RoundTable(seats);
        ITable actual = factory.create(type, seats);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void createRectangleTableTest() throws Exception {
        //given
        Integer seats = 10;
        TableType type = TableType.RECTANGLE;

        //when
        ITable expected = new RectangleTable(seats);
        ITable actual = factory.create(type, seats);

        //then
        assertEquals(expected, actual);
    }
}
