package org.weddingtables.model.table;

import org.junit.Test;
import org.weddingtables.exceptions.AlreadyOccupiedSeatException;
import org.weddingtables.exceptions.CannotRemoveGuestFromTableException;
import org.weddingtables.exceptions.InvalidDataException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by Paweł on 2017-07-10.
 */
public class TableTest {
    @Test
    public void getFreeSeatsNumberTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        Integer seats = 4;
        Integer guestsNumber = 3;
        ITable table = new RectangleTable(seats);

        //when
        for (Integer i = 0; i < guestsNumber; i++) {
            GuestUnit guest = new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i));
            table.addGuest(guest);
            table.placeGuest((Guest) guest, i);
        }
        Integer expected = seats - guestsNumber;

        //then
        assertEquals(expected, table.getFreeSeatsNumber());
    }

    @Test
    public void addGuestTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        ITable table = new RectangleTable(2);
        GuestUnit guest = new Guest(firstname, lastname);

        //when
        table.addGuest(guest);

        //then
        assertTrue(table.getGuests().contains(guest));
        assertEquals(table, guest.getTable());
    }

    @Test
    public void addGuestBelongingToTheGroupTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        ITable table = new RectangleTable(2);

        //when
        GuestUnit guest = new Guest(firstname, lastname);

        ArrayList<GuestUnit> members = new ArrayList<>();
        members.add(guest);
        GuestUnit group = new GuestGroup(members);
        table.addGuest(guest);

        //then
        assertTrue(table.getGuests().contains(group));
        assertEquals(table, guest.getTable());
    }

    @Test
    public void removeGuestTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        ITable table = new RectangleTable(2);
        GuestUnit guest = new Guest(firstname, lastname);

        //when
        table.addGuest(guest);
        table.removeGuest(guest);

        //then
        assertFalse(table.getGuests().contains(guest));
        assertEquals(null, guest.getTable());
    }

    @Test
    public void placeGuestOnValidSeatTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        Integer place = 0;
        ITable table = new RectangleTable(place + 2);
        Guest guest = new Guest(firstname, lastname);

        //when
        table.addGuest(guest);
        table.placeGuest(guest, place);

        //then
        assertEquals(guest, table.getSeats()[place]);
    }

    @Test
    public void removePlacedGuestTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        Integer place = 0;
        ITable table = new RectangleTable(place + 2);
        Guest guest = new Guest(firstname, lastname);

        //when
        table.addGuest(guest);
        table.placeGuest(guest, place);
        table.removeGuest(guest);

        //then
        assertEquals(null, table.getSeats()[place]);
    }

    @Test(expected = CannotRemoveGuestFromTableException.class)
    public void removeGuestUnassignedToTableTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        Integer place = 0;
        ITable table = new RectangleTable(place + 2);
        Guest guest = new Guest(firstname, lastname);

        //when
        table.removeGuest(guest);
    }

    @Test(expected = InvalidDataException.class)
    public void placeGuestOnOutOfRangeSeatTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        Integer seats = 2;
        ITable table = new RectangleTable(seats);
        Guest guest = new Guest(firstname, lastname);

        //when
        table.addGuest(guest);
        table.placeGuest(guest, seats + 1);
    }

    @Test(expected = InvalidDataException.class)
    public void placeGuestOnSeatWithNegativeIndexTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        Integer seats = 2;
        ITable table = new RectangleTable(seats);
        Guest guest = new Guest(firstname, lastname);

        //when
        table.addGuest(guest);
        table.placeGuest(guest, -seats);
    }

    @Test(expected = AlreadyOccupiedSeatException.class)
    public void placeGuestOnAlreadyOccupiedSeatTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        Integer place = 1;
        Integer guests = 2;
        ITable table = new RectangleTable(place + 1);

        //when
        for (Integer i = 0; i < guests; i++) {
            Guest guest = new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i));
            table.addGuest(guest);
            table.placeGuest(guest, place);
        }
    }
}
