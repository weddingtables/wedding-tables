package org.weddingtables.parser.file;

import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.weddingtables.exceptions.InvalidInputFormatException;
import org.weddingtables.model.ballroom.BallRoom;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.TableFactory;
import org.weddingtables.model.table.TableType;
import org.weddingtables.parser.file.InputFileParser;

import java.io.StringReader;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-05.
 */
public class InputFileParserTest {

    @Test
    public void parseListOfGuestTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "[{\n" +
                           "\"type\": \"GROUP\",\n" +
                           "\"members\": [{\n" +
                               "\"type\": \"INDIVIDUAL\",\n" +
                               "\"firstname\": \"Mother\",\n" +
                               "\"lastname\": \"Smith\"\n" +
                             "},\n" +
                             "{\n" +
                               "\"type\": \"INDIVIDUAL\",\n" +
                               "\"firstname\": \"Father\",\n" +
                               "\"lastname\": \"Smith\"\n" +
                             "},\n" +
                             "{\n" +
                               "\"type\": \"GROUP\",\n" +
                               "\"members\": [{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Son\",\n" +
                                   "\"lastname\": \"Smith\"\n" +
                                 "},\n" +
                                 "{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Daughter\",\n" +
                                   "\"lastname\": \"Smith\"\n" +
                                 "}\n" +
                               "]\n" +
                             "}\n" +
                           "]\n" +
                         "},\n" +
                         "{\n" +
                           "\"type\": \"INDIVIDUAL\",\n" +
                           "\"firstname\": \"Forever\",\n" +
                           "\"lastname\": \"Alone\"\n" +
                         "}\n" +
                       "]";
        StringReader reader = new StringReader(input);
        ArrayList<GuestUnit> guests = new ArrayList<>();

        //when
        ArrayList<GuestUnit> kids = new ArrayList<>();
        kids.add(new Guest("Son", "Smith"));
        kids.add(new Guest("Daughter", "Smith"));

        ArrayList<GuestUnit> familyMembers = new ArrayList<>();
        familyMembers.add(new Guest("Mother", "Smith"));
        familyMembers.add(new Guest("Father", "Smith"));
        familyMembers.add(new GuestGroup(kids));

        guests.add(new GuestGroup(familyMembers));
        guests.add(new Guest("Forever", "Alone"));

        BallRoom expected = new BallRoom(null, null, guests);
        BallRoom actual = parser.parse(reader);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void parseOutputWithUnplacedGuestsTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "{\n" +
                         "\"newlywedsTable\": {\n" +
                           "\"type\": \"RECTANGLE\",\n" +
                           "\"seats\": 2,\n" +
                           "\"guests\": [{\n" +
                               "\"type\": \"GROUP\",\n" +
                               "\"members\": [{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Groom\",\n" +
                                   "\"lastname\": \"JustMarried\"\n" +
                                 "},\n" +
                                 "{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Bride\",\n" +
                                   "\"lastname\": \"JustMarried\"\n" +
                                 "}\n" +
                               "]\n" +
                             "}\n" +
                           "]\n" +
                         "},\n" +
                         "\"guestsTables\": [{\n" +
                             "\"type\": \"ROUND\",\n" +
                             "\"seats\": 6,\n" +
                             "\"guests\": [{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname1\",\n" +
                                 "\"lastname\": \"lastname1\"\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname2\",\n" +
                                 "\"lastname\": \"lastname2\"\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname3\",\n" +
                                 "\"lastname\": \"lastname3\"\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname4\",\n" +
                                 "\"lastname\": \"lastname4\"\n" +
                               "}\n" +
                             "]\n" +
                           "},\n" +
                           "{\n" +
                             "\"type\": \"ROUND\",\n" +
                             "\"seats\": 6,\n" +
                             "\"guests\": [{\n" +
                                 "\"type\": \"GROUP\",\n" +
                                 "\"members\": [{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend0\",\n" +
                                     "\"lastname\": \"friend0\"\n" +
                                   "},\n" +
                                   "{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend1\",\n" +
                                     "\"lastname\": \"friend1\"\n" +
                                   "},\n" +
                                   "{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend2\",\n" +
                                     "\"lastname\": \"friend2\"\n" +
                                   "},\n" +
                                   "{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend3\",\n" +
                                     "\"lastname\": \"friend3\"\n" +
                                   "},\n" +
                                   "{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend4\",\n" +
                                     "\"lastname\": \"friend4\"\n" +
                                   "}\n" +
                                 "]\n" +
                               "}\n" +
                             "]\n" +
                           "}\n" +
                         "],\n" +
                         "\"unplacedGuests\": [{\n" +
                             "\"type\": \"INDIVIDUAL\",\n" +
                             "\"firstname\": \"One\",\n" +
                             "\"lastname\": \"Last\"\n" +
                           "}\n" +
                         "]\n" +
                       "}";
        StringReader reader = new StringReader(input);
        ArrayList<GuestUnit> guests = new ArrayList<>();
        Integer guestsTablesNumber = 2;
        Integer guestsTableSeats = 6;
        TableType guestTablesType = TableType.ROUND;
        Integer randomGuestsTable = 0;
        Integer numberOfRandomGuests = 4;
        String firstname = "firstname";
        String lastname = "lastname";
        Integer friendsTable = 1;
        Integer numberOfFriends = 5;
        String friendName = "friend";
        Integer newlywedsTableSeats = 2;
        TableType newlywedsTableType = TableType.RECTANGLE;
        TableFactory factory = new TableFactory();

        //when
        ITable newlywedsTable = factory.create(newlywedsTableType, newlywedsTableSeats);
        ArrayList<GuestUnit> newlywedsMembers = new ArrayList<>();
        Guest groom = new Guest("Groom", "JustMarried");
        newlywedsMembers.add(groom);
        Guest bride = new Guest("Bride", "JustMarried");
        newlywedsMembers.add(bride);
        GuestGroup newlyweds = new GuestGroup(newlywedsMembers);
        newlywedsTable.addGuest(newlyweds);
        guests.add(newlyweds);

        ITable[] guestsTables = new ITable[guestsTablesNumber];
        for (Integer i = 0; i < guestsTablesNumber; i++) {
            guestsTables[i] = factory.create(guestTablesType, guestsTableSeats);
        }

        for (Integer i = 1; i <= numberOfRandomGuests; i++) {
            Guest guest = new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i));
            guestsTables[randomGuestsTable].addGuest(guest);
            guests.add(guest);
        }

        ArrayList<GuestUnit> friendsMembers = new ArrayList<>();
        for (Integer i = 0; i < numberOfFriends; i++) {
            Guest guest = new Guest(friendName + String.valueOf(i), friendName + String.valueOf(i));
            friendsMembers.add(guest);
        }
        GuestGroup friends = new GuestGroup(friendsMembers);
        guestsTables[friendsTable].addGuest(friends);
        guests.add(friends);

        guests.add(new Guest("One", "Last"));

        BallRoom expected = new BallRoom(newlywedsTable, guestsTables, guests);
        BallRoom actual = parser.parse(reader);

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void parseOutputWithoutUnplacedGuestsTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "{\n" +
                         "\"newlywedsTable\": {\n" +
                           "\"type\": \"RECTANGLE\",\n" +
                           "\"seats\": 2,\n" +
                           "\"guests\": [{\n" +
                               "\"type\": \"GROUP\",\n" +
                               "\"members\": [{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Groom\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 0\n" +
                                 "},\n" +
                                 "{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Bride\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 1\n" +
                                 "}\n" +
                               "]\n" +
                             "}\n" +
                           "]\n" +
                         "},\n" +
                         "\"guestsTables\": [{\n" +
                             "\"type\": \"ROUND\",\n" +
                             "\"seats\": 6,\n" +
                             "\"guests\": [{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname1\",\n" +
                                 "\"lastname\": \"lastname1\",\n" +
                                 "\"place\": 1\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname2\",\n" +
                                 "\"lastname\": \"lastname2\",\n" +
                                 "\"place\": 2\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname3\",\n" +
                                 "\"lastname\": \"lastname3\",\n" +
                                 "\"place\": 3\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname4\",\n" +
                                 "\"lastname\": \"lastname4\",\n" +
                                 "\"place\": 4\n" +
                               "}\n" +
                             "]\n" +
                           "},\n" +
                           "{\n" +
                             "\"type\": \"ROUND\",\n" +
                             "\"seats\": 6,\n" +
                             "\"guests\": [{\n" +
                                 "\"type\": \"GROUP\",\n" +
                                 "\"members\": [{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend0\",\n" +
                                     "\"lastname\": \"friend0\",\n" +
                                     "\"place\": 0\n" +
                                   "},\n" +
                                   "{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend1\",\n" +
                                     "\"lastname\": \"friend1\",\n" +
                                     "\"place\": 1\n" +
                                   "},\n" +
                                   "{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend2\",\n" +
                                     "\"lastname\": \"friend2\",\n" +
                                     "\"place\": 2\n" +
                                   "},\n" +
                                   "{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend3\",\n" +
                                     "\"lastname\": \"friend3\",\n" +
                                     "\"place\": 3\n" +
                                   "},\n" +
                                   "{\n" +
                                     "\"type\": \"INDIVIDUAL\",\n" +
                                     "\"firstname\": \"friend4\",\n" +
                                     "\"lastname\": \"friend4\",\n" +
                                     "\"place\": 4\n" +
                                   "}\n" +
                                 "]\n" +
                               "}\n" +
                             "]\n" +
                           "}\n" +
                         "]\n" +
                       "}";
        StringReader reader = new StringReader(input);
        ArrayList<GuestUnit> guests = new ArrayList<>();
        Integer guestsTablesNumber = 2;
        Integer guestsTableSeats = 6;
        TableType guestTablesType = TableType.ROUND;
        Integer randomGuestsTable = 0;
        Integer numberOfRandomGuests = 4;
        String firstname = "firstname";
        String lastname = "lastname";
        Integer friendsTable = 1;
        Integer numberOfFriends = 5;
        String friendName = "friend";
        Integer newlywedsTableSeats = 2;
        TableType newlywedsTableType = TableType.RECTANGLE;
        TableFactory factory = new TableFactory();

        //when
        ITable newlywedsTable = factory.create(newlywedsTableType, newlywedsTableSeats);
        ArrayList<GuestUnit> newlywedsMembers = new ArrayList<>();
        Guest groom = new Guest("Groom", "JustMarried");
        newlywedsMembers.add(groom);
        newlywedsTable.placeGuest(groom, 0);
        Guest bride = new Guest("Bride", "JustMarried");
        newlywedsMembers.add(bride);
        newlywedsTable.placeGuest(bride, 1);
        GuestGroup newlyweds = new GuestGroup(newlywedsMembers);
        newlywedsTable.addGuest(newlyweds);
        guests.add(newlyweds);

        ITable[] guestsTables = new ITable[guestsTablesNumber];
        for (Integer i = 0; i < guestsTablesNumber; i++) {
            guestsTables[i] = factory.create(guestTablesType, guestsTableSeats);
        }

        for (Integer i = 1; i <= numberOfRandomGuests; i++) {
            Guest guest = new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i));
            guestsTables[randomGuestsTable].addGuest(guest);
            guestsTables[randomGuestsTable].placeGuest(guest, i);
            guests.add(guest);
        }

        ArrayList<GuestUnit> friendsMembers = new ArrayList<>();
        for (Integer i = 0; i < numberOfFriends; i++) {
            Guest guest = new Guest(friendName + String.valueOf(i), friendName + String.valueOf(i));
            guestsTables[friendsTable].placeGuest(guest, i);
            friendsMembers.add(guest);
        }
        GuestGroup friends = new GuestGroup(friendsMembers);
        guestsTables[friendsTable].addGuest(friends);
        guests.add(friends);

        BallRoom expected = new BallRoom(newlywedsTable, guestsTables, guests);
        BallRoom actual = parser.parse(reader);

        //then
        assertEquals(expected, actual);
    }

    @Test(expected = ParseException.class)
    public void parseNonJSONInputTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "Sample non-JSON input";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseInputOtherThanJSONArrayOrJSONObjectTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "\"hello\"";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseListOfGuestWithOneGuestNotInstanceOfJSONObjectTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "[{\n" +
                           "\"type\": \"INDIVIDUAL\",\n" +
                           "\"firstname\": \"firstname\",\n" +
                           "\"lastname\": \"lastname\"\n" +
                         "},\n" +
                         "\"second\"\n" +
                       "]";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseOutputWithoutNewlywedsTableTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "{\n" +
                         "\"guestsTables\": [{\n" +
                             "\"type\": \"ROUND\",\n" +
                             "\"seats\": 6,\n" +
                             "\"guests\": [{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname1\",\n" +
                                 "\"lastname\": \"lastname1\",\n" +
                                 "\"place\": 1\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname2\",\n" +
                                 "\"lastname\": \"lastname2\",\n" +
                                 "\"place\": 2\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname3\",\n" +
                                 "\"lastname\": \"lastname3\",\n" +
                                 "\"place\": 3\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname4\",\n" +
                                 "\"lastname\": \"lastname4\",\n" +
                                 "\"place\": 4\n" +
                               "}\n" +
                             "]\n" +
                           "}\n" +
                         "],\n" +
                         "\"unplacedGuests\": [{\n" +
                             "\"type\": \"INDIVIDUAL\",\n" +
                             "\"firstname\": \"firstname\",\n" +
                             "\"lastname\": \"lastname\"\n" +
                           "}\n" +
                         "]\n" +
                       "}";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseOutputWithNewlywedsTableNotInstanceOfJSONObjectTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "{\n" +
                          "\"newlywedsTable\": \"table\",\n" +
                          "\"guestsTables\": [{\n" +
                              "\"type\": \"ROUND\",\n" +
                              "\"seats\": 6,\n" +
                              "\"guests\": [{\n" +
                                  "\"type\": \"INDIVIDUAL\",\n" +
                                  "\"firstname\": \"firstname1\",\n" +
                                  "\"lastname\": \"lastname1\",\n" +
                                  "\"place\": 1\n" +
                                "},\n" +
                                "{\n" +
                                  "\"type\": \"INDIVIDUAL\",\n" +
                                  "\"firstname\": \"firstname2\",\n" +
                                  "\"lastname\": \"lastname2\",\n" +
                                  "\"place\": 2\n" +
                                "},\n" +
                                "{\n" +
                                  "\"type\": \"INDIVIDUAL\",\n" +
                                  "\"firstname\": \"firstname3\",\n" +
                                  "\"lastname\": \"lastname3\",\n" +
                                  "\"place\": 3\n" +
                                "},\n" +
                                "{\n" +
                                  "\"type\": \"INDIVIDUAL\",\n" +
                                  "\"firstname\": \"firstname4\",\n" +
                                  "\"lastname\": \"lastname4\",\n" +
                                  "\"place\": 4\n" +
                                "}\n" +
                              "]\n" +
                            "}\n" +
                          "],\n" +
                          "\"unplacedGuests\": [{\n" +
                            "\"type\": \"INDIVIDUAL\",\n" +
                            "\"firstname\": \"firstname\",\n" +
                            "\"lastname\": \"lastname\"\n" +
                            "}\n" +
                          "]\n" +
                        "}";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseOutputWithoutGuestsTablesTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "{\n" +
                         "\"newlywedsTable\": {\n" +
                           "\"type\": \"RECTANGLE\",\n" +
                           "\"seats\": 2,\n" +
                           "\"guests\": [{\n" +
                               "\"type\": \"GROUP\",\n" +
                               "\"members\": [{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Groom\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 0\n" +
                                 "},\n" +
                                 "{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Bride\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 1\n" +
                                 "}\n" +
                               "]\n" +
                             "}\n" +
                           "]\n" +
                         "},\n" +
                         "\"unplacedGuests\": [{\n" +
                             "\"type\": \"INDIVIDUAL\",\n" +
                             "\"firstname\": \"firstname\",\n" +
                             "\"lastname\": \"lastname\"\n" +
                           "}\n" +
                         "]\n" +
                       "}";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseOutputWithGuestsTablesNotInstanceOfJSONArrayTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "{\n" +
                        "\"newlywedsTable\": {\n" +
                           "\"type\": \"RECTANGLE\",\n" +
                           "\"seats\": 2,\n" +
                           "\"guests\": [{\n" +
                               "\"type\": \"GROUP\",\n" +
                               "\"members\": [{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Groom\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 0\n" +
                                 "},\n" +
                                 "{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Bride\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 1\n" +
                                 "}\n" +
                               "]\n" +
                             "}\n" +
                           "]\n" +
                         "},\n" +
                         "\"guestsTables\": \"table\",\n" +
                         "\"unplacedGuests\": [{\n" +
                             "\"type\": \"INDIVIDUAL\",\n" +
                             "\"firstname\": \"firstname\",\n" +
                             "\"lastname\": \"lastname\"\n" +
                           "}\n" +
                         "]\n" +
                       "}";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseOutputWithOneGuestTableNotInstanceOfJSONObjectTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "{\n" +
                         "\"newlywedsTable\": {\n" +
                           "\"type\": \"RECTANGLE\",\n" +
                           "\"seats\": 2,\n" +
                           "\"guests\": [{\n" +
                               "\"type\": \"GROUP\",\n" +
                               "\"members\": [{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Groom\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 0\n" +
                                 "},\n" +
                                 "{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Bride\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 1\n" +
                                 "}\n" +
                               "]\n" +
                             "}\n" +
                           "]\n" +
                         "},\n" +
                         "\"guestsTables\": [{\n" +
                             "\"type\": \"ROUND\",\n" +
                             "\"seats\": 6,\n" +
                             "\"guests\": [{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname1\",\n" +
                                 "\"lastname\": \"lastname1\",\n" +
                                 "\"place\": 1\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname2\",\n" +
                                 "\"lastname\": \"lastname2\",\n" +
                                 "\"place\": 2\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname3\",\n" +
                                 "\"lastname\": \"lastname3\",\n" +
                                 "\"place\": 3\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname4\",\n" +
                                 "\"lastname\": \"lastname4\",\n" +
                                 "\"place\": 4\n" +
                               "}\n" +
                             "]\n" +
                           "},\n" +
                           "\"table2\"\n" +
                         "]\n" +
                       "}";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseOutputWithUnplacedGuestsNotInstanceOfJSONArrayTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "{\n" +
                         "\"newlywedsTable\": {\n" +
                           "\"type\": \"RECTANGLE\",\n" +
                           "\"seats\": 2,\n" +
                           "\"guests\": [{\n" +
                               "\"type\": \"GROUP\",\n" +
                               "\"members\": [{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Groom\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 0\n" +
                                 "},\n" +
                                 "{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Bride\",\n" +
                                   "\"lastname\": \"JustMarried\",\n" +
                                   "\"place\": 1\n" +
                                 "}\n" +
                               "]\n" +
                             "}\n" +
                           "]\n" +
                         "},\n" +
                         "\"guestsTables\": [{\n" +
                             "\"type\": \"ROUND\",\n" +
                             "\"seats\": 6,\n" +
                             "\"guests\": [{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname1\",\n" +
                                 "\"lastname\": \"lastname1\",\n" +
                                 "\"place\": 1\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname2\",\n" +
                                 "\"lastname\": \"lastname2\",\n" +
                                 "\"place\": 2\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname3\",\n" +
                                 "\"lastname\": \"lastname3\",\n" +
                                 "\"place\": 3\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname4\",\n" +
                                 "\"lastname\": \"lastname4\",\n" +
                                 "\"place\": 4\n" +
                               "}\n" +
                             "]\n" +
                           "}\n" +
                         "],\n" +
                         "\"unplacedGuests\": \"John Doe\"\n" +
                       "}";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseOutputWithOneUnplacedGuestNotInstanceOfJSONObjectTest() throws Exception {
        //given
        InputFileParser parser = new InputFileParser();
        String input = "{\n" +
                         "\"newlywedsTable\": {\n" +
                           "\"type\": \"RECTANGLE\",\n" +
                           "\"seats\": 2,\n" +
                           "\"guests\": [{\n" +
                               "\"type\": \"GROUP\",\n" +
                               "\"members\": [{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Groom\",\n" +
                                   "\"lastname\": \"JustMarried\"\n" +
                                 "},\n" +
                                 "{\n" +
                                   "\"type\": \"INDIVIDUAL\",\n" +
                                   "\"firstname\": \"Bride\",\n" +
                                   "\"lastname\": \"JustMarried\"\n" +
                                 "}\n" +
                               "]\n" +
                             "}\n" +
                           "]\n" +
                         "},\n" +
                         "\"guestsTables\": [{\n" +
                             "\"type\": \"ROUND\",\n" +
                             "\"seats\": 6,\n" +
                             "\"guests\": [{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname1\",\n" +
                                 "\"lastname\": \"lastname1\"\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname2\",\n" +
                                 "\"lastname\": \"lastname2\"\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname3\",\n" +
                                 "\"lastname\": \"lastname3\"\n" +
                               "},\n" +
                               "{\n" +
                                 "\"type\": \"INDIVIDUAL\",\n" +
                                 "\"firstname\": \"firstname4\",\n" +
                                 "\"lastname\": \"lastname4\"\n" +
                               "}\n" +
                             "]\n" +
                           "}\n" +
                         "],\n" +
                         "\"unplacedGuests\": [{\n" +
                             "\"type\": \"INDIVIDUAL\",\n" +
                             "\"firstname\": \"One\",\n" +
                             "\"lastname\": \"Last\"\n" +
                           "},\n" +
                           "\"John Doe\"\n" +
                         "]\n" +
                       "}";
        StringReader reader = new StringReader(input);

        //when
        parser.parse(reader);
    }
}
