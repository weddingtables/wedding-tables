package org.weddingtables.parser.file;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.weddingtables.exceptions.InvalidInputFormatException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.guest.GuestUnitType;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.Table;
import org.weddingtables.model.table.TableFactory;
import org.weddingtables.model.table.TableType;
import org.weddingtables.parser.file.TableParser;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-05.
 */
public class TableParserTest {
    @Test
    public void parseTableWithZeroGuestsTest() throws Exception {
        //given
        TableType type = TableType.ROUND;
        Integer seats = 4;
        JSONObject input = new JSONObject();
        JSONArray guests = new JSONArray();
        TableFactory factory = new TableFactory();
        TableParser parser = new TableParser();

        //when
        input.put(Table.TYPE, type.name());
        input.put(Table.SEATS, seats);
        input.put(Table.GUESTS, guests);
        ITable expected = factory.create(type, seats);

        //then
        assertEquals(expected, parser.parse(input));
    }

    @Test
    public void parseTableWithGuestsTest() throws Exception {
        //given
        TableType type = TableType.ROUND;
        Integer seats = 4;
        Integer guestNumber = 3;
        String firstname = "firstname";
        String lastname = "lastname";
        JSONObject input = new JSONObject();
        JSONArray guests = new JSONArray();
        TableFactory factory = new TableFactory();
        TableParser parser = new TableParser();

        //when
        ITable expected = factory.create(type, seats);
        for (Integer i = 0; i < guestNumber; i++) {
            JSONObject guest = new JSONObject();
            guest.put(GuestUnit.TYPE, GuestUnitType.INDIVIDUAL.name());
            guest.put(GuestUnit.FIRSTNAME, firstname + String.valueOf(i));
            guest.put(GuestUnit.LASTNAME, lastname + String.valueOf(i));

            guests.add(guest);
            expected.addGuest(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }

        input.put(Table.TYPE, type.name());
        input.put(Table.SEATS, seats);
        input.put(Table.GUESTS, guests);

        //then
        assertEquals(expected, parser.parse(input));
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseTableWithoutTypeTest() throws Exception {
        //given
        Integer seats = 4;
        JSONObject input = new JSONObject();
        JSONArray guests = new JSONArray();
        TableParser parser = new TableParser();

        //when
        input.put(Table.SEATS, seats);
        input.put(Table.GUESTS, guests);

        //then
        parser.parse(input);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseTableWithTypeNotInstanceOfStringTest() throws Exception {
        //given
        TableType type = TableType.ROUND;
        Integer seats = 4;
        JSONObject input = new JSONObject();
        JSONArray guests = new JSONArray();
        TableParser parser = new TableParser();

        //when
        input.put(Table.TYPE, type);
        input.put(Table.SEATS, seats);
        input.put(Table.GUESTS, guests);

        //then
        parser.parse(input);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseTableWithInvalidTypeTest() throws Exception {
        //given
        String type = "GROUP";
        Integer seats = 4;
        JSONObject input = new JSONObject();
        JSONArray guests = new JSONArray();
        TableParser parser = new TableParser();

        //when
        input.put(Table.TYPE, type);
        input.put(Table.SEATS, seats);
        input.put(Table.GUESTS, guests);

        //then
        parser.parse(input);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseTableWithoutSeatsTest() throws Exception {
        //given
        TableType type = TableType.ROUND;
        JSONObject input = new JSONObject();
        JSONArray guests = new JSONArray();
        TableParser parser = new TableParser();

        //when
        input.put(Table.TYPE, type.name());
        input.put(Table.GUESTS, guests);

        //then
        parser.parse(input);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseTableWithSeatsNotInstanceOfIntegerTest() throws Exception {
        //given
        TableType type = TableType.ROUND;
        String seats = "5";
        JSONObject input = new JSONObject();
        JSONArray guests = new JSONArray();
        TableParser parser = new TableParser();

        //when
        input.put(Table.TYPE, type.name());
        input.put(Table.SEATS, seats);
        input.put(Table.GUESTS, guests);

        //then
        parser.parse(input);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseTableWithoutGuestsTest() throws Exception {
        //given
        TableType type = TableType.ROUND;
        Integer seats = 4;
        JSONObject input = new JSONObject();
        TableParser parser = new TableParser();

        //when
        input.put(Table.TYPE, type.name());
        input.put(Table.SEATS, seats);

        //then
        parser.parse(input);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseTableWithGuestsNotInstanceOfJSONArrayTest() throws Exception {
        //given
        TableType type = TableType.ROUND;
        Integer seats = 4;
        JSONObject input = new JSONObject();
        String guests = "[]";
        TableParser parser = new TableParser();

        //when
        input.put(Table.TYPE, type.name());
        input.put(Table.SEATS, seats);
        input.put(Table.GUESTS, guests);

        //then
        parser.parse(input);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseTableWithGuestsWhereOneIsNotInstanceOfJSONObjectTest() throws Exception {
        //given
        TableType type = TableType.ROUND;
        Integer seats = 4;
        JSONObject input = new JSONObject();
        String firstname = "firstname";
        String lastname = "lastname";
        JSONObject guest = new JSONObject();
        JSONArray guests = new JSONArray();
        TableParser parser = new TableParser();

        //when
        guest.put(GuestUnit.TYPE, GuestUnitType.INDIVIDUAL);
        guest.put(GuestUnit.FIRSTNAME, firstname);
        guest.put(GuestUnit.LASTNAME, lastname);

        guests.add(guest);
        guests.add("wrong");

        input.put(Table.TYPE, type.name());
        input.put(Table.SEATS, seats);
        input.put(Table.GUESTS, guests);

        //then
        parser.parse(input);
    }
}
