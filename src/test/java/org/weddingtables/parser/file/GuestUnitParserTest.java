package org.weddingtables.parser.file;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.weddingtables.exceptions.IncompleteDataException;
import org.weddingtables.exceptions.InvalidDataException;
import org.weddingtables.exceptions.InvalidInputFormatException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.guest.GuestUnitType;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.RectangleTable;
import org.weddingtables.parser.file.GuestUnitParser;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-05.
 */
public class GuestUnitParserTest {

    @Test(expected = InvalidInputFormatException.class)
    public void parseGuestUnitWithoutTypeTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        input.put(GuestUnit.FIRSTNAME, firstname);
        input.put(Guest.LASTNAME, lastname);

        //then
        parser.parse(input, null);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseGuestUnitWithNotInstanceOfStringTypeTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        input.put(GuestUnit.TYPE, type);
        input.put(GuestUnit.FIRSTNAME, firstname);
        input.put(Guest.LASTNAME, lastname);

        //then
        parser.parse(input, null);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseGuestUnitWithInvalidTypeTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        String type = "TABLE";
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        input.put(GuestUnit.TYPE, type);
        input.put(GuestUnit.FIRSTNAME, firstname);
        input.put(Guest.LASTNAME, lastname);

        //then
        parser.parse(input, null);
    }

    @Test
    public void parseIndividualWithoutTableTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        input.put(GuestUnit.TYPE, type.name());
        input.put(GuestUnit.FIRSTNAME, firstname);
        input.put(Guest.LASTNAME, lastname);

        GuestUnit expected = new Guest(firstname, lastname);

        //then
        assertEquals(expected, parser.parse(input, null));
    }

    @Test
    public void parseIndividualWithTableAndAssignedSeatTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();
        Integer place = 1;
        ITable table = new RectangleTable(place + 1);

        //when
        input.put(GuestUnit.TYPE, type.name());
        input.put(GuestUnit.FIRSTNAME, firstname);
        input.put(Guest.LASTNAME, lastname);
        input.put(GuestUnit.PLACE, place);

        GuestUnit expected = new Guest(firstname, lastname);

        //then
        assertEquals(expected, parser.parse(input, table));
        assertEquals(expected, table.getSeats()[place]);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseIndividualWithoutFirstnameTest() throws Exception {
        //given
        String lastname = "lastname";
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        input.put(GuestUnit.TYPE, type.name());
        input.put(Guest.LASTNAME, lastname);

        //then
        parser.parse(input, null);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseIndividualWithFirstnameNotInstanceOfStringTest() throws Exception {
        //given
        Integer firstname = 5;
        String lastname = "lastname";
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        input.put(GuestUnit.TYPE, type.name());
        input.put(GuestUnit.FIRSTNAME, firstname);
        input.put(Guest.LASTNAME, lastname);

        //then
        parser.parse(input, null);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseIndividualWithoutLastnameTest() throws Exception {
        //given
        String firstname = "firstname";
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        input.put(GuestUnit.TYPE, type.name());
        input.put(Guest.FIRSTNAME, firstname);

        //then
        parser.parse(input, null);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseIndividualWithLastnameNotInstanceOfStringTest() throws Exception {
        //given
        String firstname = "firstname";
        Integer lastname = 5;
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        input.put(GuestUnit.TYPE, type.name());
        input.put(GuestUnit.FIRSTNAME, firstname);
        input.put(Guest.LASTNAME, lastname);

        //then
        parser.parse(input, null);
    }

    @Test(expected = InvalidDataException.class)
    public void parseIndividualWithoutTableButWithAssignedSeatTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();
        Integer place = 1;

        //when
        input.put(GuestUnit.TYPE, type.name());
        input.put(GuestUnit.FIRSTNAME, firstname);
        input.put(Guest.LASTNAME, lastname);
        input.put(GuestUnit.PLACE, place);

        //then
        parser.parse(input, null);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseIndividualWithTableAndPlaceNotInstanceOfIntegerTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();
        Integer seats = 2;
        String place = "1";
        ITable table = new RectangleTable(seats);

        //when
        input.put(GuestUnit.TYPE, type.name());
        input.put(GuestUnit.FIRSTNAME, firstname);
        input.put(Guest.LASTNAME, lastname);
        input.put(GuestUnit.PLACE, place);

        //then
        parser.parse(input, table);
    }

    @Test(expected = InvalidDataException.class)
    public void parseTwoIndividualsWithTableAndAssignedToSameSeatTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        GuestUnitType type = GuestUnitType.INDIVIDUAL;
        JSONObject input1 = new JSONObject();
        JSONObject input2 = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();
        Integer place = 1;
        ITable table = new RectangleTable(place + 1);

        //when
        input1.put(GuestUnit.TYPE, type.name());
        input1.put(GuestUnit.FIRSTNAME, firstname + "1");
        input1.put(Guest.LASTNAME, lastname + "1");
        input1.put(GuestUnit.PLACE, place);

        input2.put(GuestUnit.TYPE, type.name());
        input2.put(GuestUnit.FIRSTNAME, firstname + "2");
        input2.put(Guest.LASTNAME, lastname + "2");
        input2.put(GuestUnit.PLACE, place);

        //then
        parser.parse(input1, table);
        parser.parse(input2, table);
    }

    @Test
    public void parseGroupWithTwoMembersWithoutTableTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        Integer membersNumber = 2;
        GuestUnitType type = GuestUnitType.GROUP;
        JSONObject groupInput = new JSONObject();
        JSONArray membersInput = new JSONArray();
        GuestUnitParser parser = new GuestUnitParser();
        ArrayList<GuestUnit> members = new ArrayList<>();

        //when
        for (Integer i = 0; i < membersNumber; i++) {
            JSONObject member = new JSONObject();
            member.put(GuestUnit.TYPE, GuestUnitType.INDIVIDUAL.name());
            member.put(GuestUnit.FIRSTNAME, firstname + String.valueOf(i));
            member.put(GuestUnit.LASTNAME, lastname + String.valueOf(i));
            membersInput.add(member);

            members.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }

        groupInput.put(GuestUnit.TYPE, type.name());
        groupInput.put(GuestUnit.MEMBERS, membersInput);
        GuestUnit expected = new GuestGroup(members);

        //then
        assertEquals(expected, parser.parse(groupInput, null));
    }

    @Test
    public void parseGroupWithTwoMembersWithTableAndSeatsAssignedTest() throws Exception {
        //given
        String firstname = "firstname";
        String lastname = "lastname";
        Integer membersNumber = 2;
        GuestUnitType type = GuestUnitType.GROUP;
        JSONObject groupInput = new JSONObject();
        JSONArray membersInput = new JSONArray();
        GuestUnitParser parser = new GuestUnitParser();
        ArrayList<GuestUnit> members = new ArrayList<>();
        ITable table = new RectangleTable(membersNumber + 1);

        //when
        for (Integer i = 0; i < membersNumber; i++) {
            JSONObject member = new JSONObject();
            member.put(GuestUnit.TYPE, GuestUnitType.INDIVIDUAL.name());
            member.put(GuestUnit.FIRSTNAME, firstname + String.valueOf(i));
            member.put(GuestUnit.LASTNAME, lastname + String.valueOf(i));
            member.put(GuestUnit.PLACE, i);
            membersInput.add(member);

            members.add(new Guest(firstname + String.valueOf(i), lastname + String.valueOf(i)));
        }

        groupInput.put(GuestUnit.TYPE, type.name());
        groupInput.put(GuestUnit.MEMBERS, membersInput);
        GuestUnit expected = new GuestGroup(members);

        //then
        assertEquals(expected, parser.parse(groupInput, table));
        for (Integer i = 0; i < members.size(); i++) {
            assertEquals(members.get(i), table.getSeats()[i]);
        }
    }

    @Test(expected = IncompleteDataException.class)
    public void parseGroupWithZeroMembersTest() throws Exception {
        //given
        GuestUnitType type = GuestUnitType.GROUP;
        JSONObject groupInput = new JSONObject();
        JSONArray membersInput = new JSONArray();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        groupInput.put(GuestUnit.TYPE, type.name());
        groupInput.put(GuestUnit.MEMBERS, membersInput);

        //then
        parser.parse(groupInput, null);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseGroupWithoutMembersTest() throws Exception {
        //given
        GuestUnitType type = GuestUnitType.GROUP;
        JSONObject groupInput = new JSONObject();
        GuestUnitParser parser = new GuestUnitParser();

        //when
        groupInput.put(GuestUnit.TYPE, type.name());

        //then
        parser.parse(groupInput, null);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseGroupWithMembersNotInstanceOfJSONArrayTest() throws Exception {
        //given
        GuestUnitType type = GuestUnitType.GROUP;
        JSONObject groupInput = new JSONObject();
        String membersInput = "[]";
        GuestUnitParser parser = new GuestUnitParser();

        //when
        groupInput.put(GuestUnit.TYPE, type.name());
        groupInput.put(GuestUnit.MEMBERS, membersInput);

        //then
        parser.parse(groupInput, null);
    }
}
