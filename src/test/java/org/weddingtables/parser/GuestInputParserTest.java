package org.weddingtables.parser;

import org.junit.Test;
import org.weddingtables.exceptions.InvalidInputFormatException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

/**
 * Created by Paweł on 2017-07-10.
 */
public class GuestInputParserTest {

    @Test
    public void parseGuestsTest() throws Exception {
        //given
        GuestInputParser parser = new GuestInputParser();
        String input = "firstname1 lastname1\n" +
                       "firstname2 lastname2\n" +
                       "firstname3 lastname3\n" +
                       "firstname4 lastname4\n";

        //when
        ArrayList<GuestUnit> expected = new ArrayList<>();
        expected.add(new Guest("firstname1", "lastname1"));
        expected.add(new Guest("firstname2", "lastname2"));
        expected.add(new Guest("firstname3", "lastname3"));
        expected.add(new Guest("firstname4", "lastname4"));

        //then
        assertEquals(expected, parser.parse(input));
    }

    @Test
    public void parseGuestGroupTest() throws Exception {
        //given
        GuestInputParser parser = new GuestInputParser();
        String input = "{\n" +
                       "\tfirstname1 lastname1\n" +
                       "\tfirstname2 lastname2\n" +
                       "\tfirstname3 lastname3\n" +
                       "\tfirstname4 lastname4\n" +
                       "}\n";

        //when
        ArrayList<GuestUnit> members = new ArrayList<>();
        members.add(new Guest("firstname1", "lastname1"));
        members.add(new Guest("firstname2", "lastname2"));
        members.add(new Guest("firstname3", "lastname3"));
        members.add(new Guest("firstname4", "lastname4"));

        ArrayList<GuestUnit> expected = new ArrayList<>();
        expected.add(new GuestGroup(members));

        //then
        assertEquals(expected, parser.parse(input));
    }

    @Test
    public void parseMixedInputTest() throws Exception {
        //given
        GuestInputParser parser = new GuestInputParser();
        String input = "firstname lastname\n" +
                       "{\n" +
                       "\tfirstname1 lastname1\n" +
                       "\tfirstname2 lastname2\n" +
                       "\tfirstname3 lastname3\n" +
                       "\tfirstname4 lastname4\n" +
                       "}\n";

        //when
        ArrayList<GuestUnit> members = new ArrayList<>();
        members.add(new Guest("firstname1", "lastname1"));
        members.add(new Guest("firstname2", "lastname2"));
        members.add(new Guest("firstname3", "lastname3"));
        members.add(new Guest("firstname4", "lastname4"));

        ArrayList<GuestUnit> expected = new ArrayList<>();
        expected.add(new Guest("firstname", "lastname"));
        expected.add(new GuestGroup(members));

        //then
        assertEquals(expected, parser.parse(input));
    }

    @Test
    public void parseNestedGuestGroupTest() throws Exception {
        //given
        GuestInputParser parser = new GuestInputParser();
        String input = "{\n" +
                       "\tfirstname lastname\n" +
                       "\t{\n" +
                       "\t\tfirstname1 lastname1\n" +
                       "\t\tfirstname2 lastname2\n" +
                       "\t\tfirstname3 lastname3\n" +
                       "\t\tfirstname4 lastname4\n" +
                       "\t}\n" +
                       "}\n";

        //when
        ArrayList<GuestUnit> nestedMembers = new ArrayList<>();
        nestedMembers.add(new Guest("firstname1", "lastname1"));
        nestedMembers.add(new Guest("firstname2", "lastname2"));
        nestedMembers.add(new Guest("firstname3", "lastname3"));
        nestedMembers.add(new Guest("firstname4", "lastname4"));

        ArrayList<GuestUnit> members = new ArrayList<>();
        members.add(new Guest("firstname", "lastname"));
        members.add(new GuestGroup(nestedMembers));

        ArrayList<GuestUnit> expected = new ArrayList<>();
        expected.add(new GuestGroup(members));

        //then
        assertEquals(expected, parser.parse(input));
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseUnclosedGuestGroupTest() throws Exception {
        //given
        GuestInputParser parser = new GuestInputParser();
        String input = "{\n" +
                       "\tfirstname1 lastname1\n" +
                       "\tfirstname2 lastname2\n";

        //when
        parser.parse(input);
    }

    @Test(expected = InvalidInputFormatException.class)
    public void parseUnopenedGuestGroupTest() throws Exception {
        //given
        GuestInputParser parser = new GuestInputParser();
        String input = "firstname1 lastname1\n" +
                       "firstname2 lastname2\n" +
                       "}\n";

        //when
        parser.parse(input);
    }
}
