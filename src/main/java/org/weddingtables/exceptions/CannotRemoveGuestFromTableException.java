package org.weddingtables.exceptions;

/**
 * Created by Paweł on 2017-07-11.
 */
public class CannotRemoveGuestFromTableException extends Exception {
    public CannotRemoveGuestFromTableException() {
        super("GuestUnit was not assigned to that table");
    }
}
