package org.weddingtables.exceptions;

/**
 * Created by Paweł on 2017-07-05.
 */
public class InvalidDataException extends Exception {
    public InvalidDataException() {
        super("Invalid data");
    }
}
