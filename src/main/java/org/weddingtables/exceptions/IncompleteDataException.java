package org.weddingtables.exceptions;

/**
 * Created by Paweł on 2017-07-02.
 */
public class IncompleteDataException extends Exception {
    public IncompleteDataException(String missingData) {
        super("missing: " + missingData);
    }
}
