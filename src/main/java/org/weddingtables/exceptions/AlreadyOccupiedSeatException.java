package org.weddingtables.exceptions;

/**
 * Created by Paweł on 2017-07-05.
 */
public class AlreadyOccupiedSeatException extends Exception {
    public AlreadyOccupiedSeatException() {
        super("Other guest is already assigned to this seat.");
    }
}
