package org.weddingtables.exceptions;

/**
 * Created by Paweł on 2017-07-04.
 */
public class InvalidInputFormatException extends Exception {
    public InvalidInputFormatException() {
        super("Invalid data format in provided input");
    }
}
