package org.weddingtables.exceptions;

/**
 * Created by Paweł on 2017-07-05.
 */
public class InvalidOutputGenerationRequestException extends Exception {
    public InvalidOutputGenerationRequestException() {
        super("Cannot generate output with prividen data.");
    }
}
