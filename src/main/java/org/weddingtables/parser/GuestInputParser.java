package org.weddingtables.parser;

import org.weddingtables.exceptions.IncompleteDataException;
import org.weddingtables.exceptions.InvalidInputFormatException;
import org.weddingtables.model.guest.*;

import java.util.ArrayList;

/**
 * Created by Paweł on 2017-07-10.
 */
public class GuestInputParser {
    private Integer lineIndex;
    private String[] lines;

    public GuestInputParser() {}

    public ArrayList<GuestUnit> parse(String input) throws InvalidInputFormatException {
        ArrayList<GuestUnit> guests = new ArrayList<>();

        input = input.replace("\t", "");
        input = input.replaceAll(" +", " ");
        lines = input.split("\n");

        for (lineIndex = 0; lineIndex < lines.length; lineIndex++) {
            String line = lines[lineIndex].trim();
            if (!line.isEmpty()) {
                if (line.equals("{")) {
                    lineIndex++;
                    guests.add(parseGroup());
                } else if (!line.equals("}")) {
                    guests.add(parseIndividual());
                } else throw new InvalidInputFormatException();
            }
        }

        return guests;
    }

    private GuestUnit parseGroup() throws InvalidInputFormatException {
        GuestUnitBuilder builder = new GuestUnitBuilder(GuestUnitType.GROUP);

        while (lineIndex != lines.length && !lines[lineIndex].equals("}")) {
            String line = lines[lineIndex].trim();
            if (!line.isEmpty()) {
                if (line.equals("{")) {
                    lineIndex++;
                    builder.addMember(parseGroup());
                } else {
                    builder.addMember(parseIndividual());
                }
            }
            lineIndex++;
        }

        if (lineIndex == lines.length) throw new InvalidInputFormatException();

        try {
            return builder.build();
        } catch (IncompleteDataException e) {
            throw new InvalidInputFormatException();
        }
    }

    private GuestUnit parseIndividual() throws InvalidInputFormatException {
        String line = lines[lineIndex].trim();
        Integer separator = line.indexOf(" ");
        if (separator == -1) throw new InvalidInputFormatException();

        GuestUnitBuilder builder = new GuestUnitBuilder(GuestUnitType.INDIVIDUAL);
        builder.setFirstname(line.substring(0, separator));
        builder.setLastname(line.substring(separator + 1));

        try {
            return builder.build();
        } catch (IncompleteDataException e) {
            throw new InvalidInputFormatException();
        }
    }
}
