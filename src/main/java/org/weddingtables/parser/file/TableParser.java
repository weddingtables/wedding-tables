package org.weddingtables.parser.file;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.weddingtables.exceptions.IncompleteDataException;
import org.weddingtables.exceptions.InvalidDataException;
import org.weddingtables.exceptions.InvalidInputFormatException;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.Table;
import org.weddingtables.model.table.TableFactory;
import org.weddingtables.model.table.TableType;
import org.weddingtables.parser.file.GuestUnitParser;

/**
 * Created by Paweł on 2017-07-04.
 */
public class TableParser {

    public TableParser() {}

    public ITable parse(JSONObject input) throws InvalidInputFormatException, InvalidDataException, IncompleteDataException {
        GuestUnitParser guestUnitParser = new GuestUnitParser();
        TableFactory tableFactory = new TableFactory();
        ITable table;

        Object tableTypeObject = input.get(Table.TYPE);
        if (tableTypeObject == null) throw new InvalidInputFormatException();
        if (!(tableTypeObject instanceof String)) throw new InvalidInputFormatException();
        TableType tableType;
        try {
            tableType = TableType.valueOf((String)tableTypeObject);
        } catch (IllegalArgumentException e) {
            throw new InvalidInputFormatException();
        }

        Object seatsObject = input.get(Table.SEATS);
        if (seatsObject == null) throw new InvalidInputFormatException();
        if (!(seatsObject instanceof Number)) throw new InvalidInputFormatException();
        table = tableFactory.create(tableType, ((Number)seatsObject).intValue());

        Object guestsObject = input.get(Table.GUESTS);
        if (guestsObject == null) throw new InvalidInputFormatException();
        if (!(guestsObject instanceof JSONArray)) throw new InvalidInputFormatException();
        for (Object guestObject : (JSONArray)guestsObject) {
            if (!(guestObject instanceof JSONObject)) throw new InvalidInputFormatException();
            GuestUnit guest = guestUnitParser.parse((JSONObject)guestObject, table);
            table.addGuest(guest);
        }

        return table;
    }
}
