package org.weddingtables.parser.file;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.weddingtables.exceptions.IncompleteDataException;
import org.weddingtables.exceptions.InvalidDataException;
import org.weddingtables.exceptions.InvalidInputFormatException;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.ballroom.BallRoom;
import org.weddingtables.parser.file.GuestUnitParser;
import org.weddingtables.parser.file.TableParser;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

/**
 * Created by Paweł on 2017-07-04.
 */
public class InputFileParser {

    public InputFileParser() {}

    public BallRoom parse(Reader in) throws IOException, ParseException,
            InvalidInputFormatException, IncompleteDataException, InvalidDataException {
        ITable newlywedTable = null;
        ArrayList<ITable> guestsTables = new ArrayList<>();
        ArrayList<GuestUnit> guests = new ArrayList<>();

        JSONParser jsonParser = new JSONParser();
        GuestUnitParser guestUnitParser = new GuestUnitParser();
        TableParser tableParser = new TableParser();

        Object input = jsonParser.parse(in);
        if (input instanceof JSONArray) {
            //no placed guests
            JSONArray listOfGuestUnits = (JSONArray) input;
            for (Object guestUnit : listOfGuestUnits) {
                if (!(guestUnit instanceof JSONObject)) throw new InvalidInputFormatException();
                guests.add(guestUnitParser.parse((JSONObject) guestUnit, null));
            }
        } else if (input instanceof JSONObject) {
            // placed guests
            JSONObject outputObject = (JSONObject) input;

            //parse newly-weds table
            Object newlywedTableObject = outputObject.get(BallRoom.NEWLYWEDSTABLE);
            if (newlywedTableObject == null) throw new InvalidInputFormatException();
            if (!(newlywedTableObject instanceof JSONObject)) throw new InvalidInputFormatException();

            newlywedTable = tableParser.parse((JSONObject) newlywedTableObject);
            guests.addAll(newlywedTable.getGuests());

            //parse guests tables
            Object guestsTableArrayObject = outputObject.get(BallRoom.GUESTSTABLES);
            if (guestsTableArrayObject == null) throw new InvalidInputFormatException();
            if (!(guestsTableArrayObject instanceof JSONArray)) throw new InvalidInputFormatException();

            for (Object guestsTableObject : (JSONArray) guestsTableArrayObject) {
                if (!(guestsTableObject instanceof JSONObject)) throw new InvalidInputFormatException();
                ITable table = tableParser.parse((JSONObject) guestsTableObject);
                guestsTables.add(table);
                guests.addAll(table.getGuests());
            }

            //parse unplaced guestUnits
            Object unplacedGuestUnitsObject = outputObject.get(BallRoom.UNPLACEDGUESTS);
            if (unplacedGuestUnitsObject != null) {
                if (!(unplacedGuestUnitsObject instanceof JSONArray)) throw new InvalidInputFormatException();

                for (Object unplacedGuestUnit : (JSONArray) unplacedGuestUnitsObject) {
                    if (!(unplacedGuestUnit instanceof JSONObject)) throw new InvalidInputFormatException();
                    guests.add(guestUnitParser.parse((JSONObject) unplacedGuestUnit, null));
                }
            }
        } else throw new InvalidInputFormatException();

        ITable[] guestsTablesArray = null;
        if (!guestsTables.isEmpty()) {
            guestsTablesArray = new ITable[guestsTables.size()];
            guestsTablesArray = guestsTables.toArray(guestsTablesArray);
        }

        return new BallRoom(newlywedTable, guestsTablesArray, guests);
    }
}
