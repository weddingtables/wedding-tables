package org.weddingtables.parser.file;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.weddingtables.exceptions.AlreadyOccupiedSeatException;
import org.weddingtables.exceptions.IncompleteDataException;
import org.weddingtables.exceptions.InvalidDataException;
import org.weddingtables.exceptions.InvalidInputFormatException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.guest.GuestUnitBuilder;
import org.weddingtables.model.guest.GuestUnitType;
import org.weddingtables.model.table.ITable;

/**
 * Created by Paweł on 2017-07-04.
 */
public class GuestUnitParser {

    public GuestUnitParser() {}

    public GuestUnit parse(JSONObject input, ITable table) throws InvalidInputFormatException,
            IncompleteDataException, InvalidDataException {
        GuestUnitParser guestUnitParser = new GuestUnitParser();

        Object guestUnitTypeObject = input.get(GuestUnit.TYPE);
        if (guestUnitTypeObject == null) throw new InvalidInputFormatException();
        if (!(guestUnitTypeObject instanceof String)) throw new InvalidInputFormatException();

        GuestUnitType guestUnitType;
        try {
            guestUnitType = GuestUnitType.valueOf((String)guestUnitTypeObject);
        } catch (IllegalArgumentException e) {
            throw new InvalidInputFormatException();
        }

        GuestUnitBuilder builder = new GuestUnitBuilder(guestUnitType);
        GuestUnit guestUnit;
        switch (guestUnitType) {
            case INDIVIDUAL:
                Object firstnameObject = input.get(GuestUnit.FIRSTNAME);
                if (firstnameObject == null) throw new InvalidInputFormatException();
                if (!(firstnameObject instanceof String)) throw new InvalidInputFormatException();
                builder.setFirstname((String)firstnameObject);

                Object lastnameObject = input.get(GuestUnit.LASTNAME);
                if (lastnameObject == null) throw new InvalidInputFormatException();
                if (!(lastnameObject instanceof String)) throw new InvalidInputFormatException();
                builder.setLastname((String)lastnameObject);

                guestUnit = builder.build();
                Object placeObject = input.get(GuestUnit.PLACE);
                if (placeObject != null) {
                    if (table != null) {
                        if (!(placeObject instanceof Number)) throw new InvalidInputFormatException();

                        try {
                            table.placeGuest((Guest) guestUnit, ((Number)placeObject).intValue());
                        } catch (AlreadyOccupiedSeatException e) {
                            throw new InvalidDataException();
                        }
                    } else throw new InvalidDataException();
                }
                break;
            case GROUP:
                Object membersObject = input.get(GuestUnit.MEMBERS);
                if (membersObject == null) throw new InvalidInputFormatException();
                if (!(membersObject instanceof JSONArray)) throw new InvalidInputFormatException();
                for (Object member : (JSONArray)membersObject) {
                    if (!(member instanceof JSONObject)) throw new InvalidInputFormatException();
                    builder.addMember(guestUnitParser.parse((JSONObject)member, table));
                }
                guestUnit = builder.build();
                break;
            default:
                throw new InvalidDataException();
        }

        return guestUnit;
    }
}
