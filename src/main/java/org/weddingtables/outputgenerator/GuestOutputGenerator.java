package org.weddingtables.outputgenerator;

import org.weddingtables.exceptions.InvalidOutputGenerationRequestException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;

import java.util.ArrayList;

/**
 * Created by Paweł on 2017-07-10.
 */
public class GuestOutputGenerator {

    public GuestOutputGenerator() {}

    public String generate(ArrayList<GuestUnit> guests, String indent) throws InvalidOutputGenerationRequestException {
        StringBuilder out = new StringBuilder();

        for (GuestUnit guestUnit : guests) {
            switch (guestUnit.getType()) {
                case INDIVIDUAL:
                    Guest guest = (Guest) guestUnit;
                    out.append(indent).append(guest.getFirstname()).append(" ").append(guest.getLastname()).append("\n");
                    break;
                case GROUP:
                    GuestGroup group = (GuestGroup) guestUnit;
                    out.append(indent).append("{\n");
                    out.append(generate(group.getMembers(), indent + "\t")).append("\n");
                    out.delete(out.length() - "\n".length(), out.length());
                    out.append(indent).append("}\n");
                    break;
                default:
                    throw new InvalidOutputGenerationRequestException();
            }
        }

        return out.toString();
    }
}
