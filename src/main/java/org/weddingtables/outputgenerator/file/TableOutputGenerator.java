package org.weddingtables.outputgenerator.file;

import org.weddingtables.exceptions.InvalidOutputGenerationRequestException;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.Table;

/**
 * Created by Paweł on 2017-07-10.
 */
public class TableOutputGenerator {

    public TableOutputGenerator() {}

    public String generate(ITable table) throws InvalidOutputGenerationRequestException {
        GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();
        StringBuilder out = new StringBuilder("{\n");

        out.append("\"").append(Table.TYPE).append("\": \"").append(table.getType().name()).append("\",\n");
        out.append("\"").append(Table.SEATS).append("\": ").append(String.valueOf(table.getSeats().length)).append(",\n");
        out.append("\"").append(Table.GUESTS).append("\": [");
        for (GuestUnit guest : table.getGuests()) {
            out.append(generator.generate(guest)).append(",\n");
        }
        if (out.toString().endsWith(",\n"))
            out.delete(out.length() - ",\n".length(), out.length());
        out.append("\n]\n}");

        return out.toString();
    }
}
