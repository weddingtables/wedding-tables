package org.weddingtables.outputgenerator.file;

import org.weddingtables.exceptions.InvalidOutputGenerationRequestException;
import org.weddingtables.model.ballroom.BallRoom;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;

/**
 * Created by Paweł on 2017-07-10.
 */
public class BallRoomOutputGenerator {
    public BallRoomOutputGenerator() {}

    public String generate(BallRoom ballRoom) throws InvalidOutputGenerationRequestException {
        if (ballRoom.getNewlywedsTable() == null || ballRoom.getGuestsTables() == null) throw new InvalidOutputGenerationRequestException();

        StringBuilder out = new StringBuilder("{\n");
        TableOutputGenerator tableOutputGenerator = new TableOutputGenerator();
        GuestUnitOutputGenerator guestUnitOutputGenerator = new GuestUnitOutputGenerator();

        //newly-wed table
        out.append("\"").append(BallRoom.NEWLYWEDSTABLE).append("\": ");
        out.append(tableOutputGenerator.generate(ballRoom.getNewlywedsTable())).append(",\n");

        //guests tables
        out.append("\"").append(BallRoom.GUESTSTABLES).append("\": [");
        for (ITable table : ballRoom.getGuestsTables()) {
            if (table == null) throw new InvalidOutputGenerationRequestException();
            out.append(tableOutputGenerator.generate(table)).append(",\n");
        }
        if (out.toString().endsWith(",\n"))
            out.delete(out.length() - ",\n".length(), out.length());
        out.append("\n]");

        //if occur, unplaced guests
        if (ballRoom.getGuests().stream().anyMatch(guest -> guest.getTable() == null)) {
            out.append(",\n");
            out.append("\"").append(BallRoom.UNPLACEDGUESTS).append("\": [");
            for (GuestUnit guest : ballRoom.getGuests()) {
                if (guest.getTable() == null) {
                    out.append(guestUnitOutputGenerator.generate(guest)).append(",\n");
                }
            }
            if (out.toString().endsWith(",\n"))
                out.delete(out.length() - ",\n".length(), out.length());
            out.append("\n]");
        }
        out.append("\n}");

        return out.toString();
    }
}
