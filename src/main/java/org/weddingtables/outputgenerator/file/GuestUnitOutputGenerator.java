package org.weddingtables.outputgenerator.file;

import org.weddingtables.exceptions.InvalidOutputGenerationRequestException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestGroup;
import org.weddingtables.model.guest.GuestUnit;

import java.util.Arrays;

/**
 * Created by Paweł on 2017-07-10.
 */
public class GuestUnitOutputGenerator {
    public GuestUnitOutputGenerator() {}

    public String generate(GuestUnit guestUnit) throws InvalidOutputGenerationRequestException {
        StringBuilder out = new StringBuilder("{\n");

        switch (guestUnit.getType()) {
            case INDIVIDUAL:
                Guest guest = (Guest) guestUnit;
                out.append("\"").append(GuestUnit.TYPE).append("\": \"").append(guestUnit.getType().name()).append("\",\n");
                out.append("\"").append(GuestUnit.FIRSTNAME).append("\": \"").append(guest.getFirstname()).append("\",\n");
                out.append("\"").append(GuestUnit.LASTNAME).append("\": \"").append(guest.getLastname()).append("\"");
                if (guest.getTable() != null)
                {
                    Integer index = Arrays.asList(guest.getTable().getSeats()).indexOf(guest);
                    if (index != -1) {
                        out.append(",\n\"").append(GuestUnit.PLACE).append("\": ").append(String.valueOf(index));
                    }
                }
                out.append("\n}");
                break;
            case GROUP:
                GuestGroup guestGroup = (GuestGroup) guestUnit;
                GuestUnitOutputGenerator generator = new GuestUnitOutputGenerator();

                out.append("\"").append(GuestUnit.TYPE).append("\": \"").append(guestGroup.getType().name()).append("\",\n");
                out.append("\"").append(GuestUnit.MEMBERS).append("\": [");
                for (GuestUnit member : guestGroup.getMembers()) {
                    out.append(generator.generate(member)).append(",\n");
                }
                if (out.toString().endsWith(",\n"))
                    out.delete(out.length() - ",\n".length(), out.length());
                out.append("\n]\n}");
                break;
            default:
                throw new InvalidOutputGenerationRequestException();
        }

        return out.toString();
    }
}
