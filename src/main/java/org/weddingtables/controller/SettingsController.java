package org.weddingtables.controller;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import org.json.simple.parser.ParseException;
import org.weddingtables.exceptions.IncompleteDataException;
import org.weddingtables.exceptions.InvalidDataException;
import org.weddingtables.exceptions.InvalidInputFormatException;
import org.weddingtables.exceptions.InvalidOutputGenerationRequestException;
import org.weddingtables.model.ballroom.BallRoom;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;
import org.weddingtables.model.table.TableFactory;
import org.weddingtables.model.table.TableType;
import org.weddingtables.outputgenerator.GuestOutputGenerator;
import org.weddingtables.parser.GuestInputParser;
import org.weddingtables.parser.file.InputFileParser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ArrayList;

/**
 * Created by Paweł on 2017-07-22.
 */
public class SettingsController {
    @FXML
    private AnchorPane table_settings_pane;
    @FXML
    private ToggleGroup table_settings;
    @FXML
    private Toggle individual;
    @FXML
    private Toggle common;
    @FXML
    private ComboBox<TableType> nw_table_type;
    @FXML
    private Spinner<Integer> number_of_guest_tables;
    @FXML
    private Spinner<Integer> nw_table_seats;
    @FXML
    private TextArea guest_input;
    @FXML
    private Button generate;

    private GridPane individualSettings;
    private ComboBox<TableType> guestsTablesCommonType;
    private Spinner<Integer> guestsTablesCommonSeats;

    @FXML
    public void initialize()
    {
        //TABLE SETTINGS SETUP
        setCommonContent();
        table_settings.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals(individual)) {
                setIndividualContent();
            } else if (newValue.equals(common)) {
                setCommonContent();
            }
        });

        //COMBOBOX SETUP
        setUpTableTypeCombobox(nw_table_type);

        //SPINNER SETUP
        setUpIntegerSpinner(nw_table_seats, 2, 100);
        setUpIntegerSpinner(number_of_guest_tables, 0, 100);
        number_of_guest_tables.valueProperty().addListener(((observable, oldValue, newValue) -> updateIndividualSettings(oldValue, newValue)));
    }

    @FXML
    public void onClick() {
        try {
            GuestInputParser parser = new GuestInputParser();
            ArrayList<GuestUnit> guests = parser.parse(guest_input.getText());

            TableFactory factory = new TableFactory();
            ITable newlywedsTable = factory.create(nw_table_type.getValue(), nw_table_seats.getValue());

            ITable[] guestsTables = new ITable[number_of_guest_tables.getValue()];
            if (table_settings.getSelectedToggle().equals(individual)) {
                for (Integer i = 0; i < number_of_guest_tables.getValue(); i++) {
                    GridPane tableInfo = (GridPane) individualSettings.getChildren().get(i);
                    ComboBox tableType = (ComboBox) tableInfo.getChildren().get(1);
                    Spinner tableSeats = (Spinner) tableInfo.getChildren().get(2);

                    guestsTables[i] = factory.create((TableType)tableType.getValue(), (Integer)tableSeats.getValue());
                }
            } else if (table_settings.getSelectedToggle().equals(common)) {
                for (Integer i = 0; i < number_of_guest_tables.getValue(); i++) {
                    guestsTables[i] = factory.create(guestsTablesCommonType.getValue(), guestsTablesCommonSeats.getValue());
                }
            }

            BallRoom ballRoom = new BallRoom(newlywedsTable, guestsTables, guests);
            showBallroom(ballRoom);
        } catch (InvalidInputFormatException e) {
            ErrorController.raiseError(e.getMessage());
            e.printStackTrace();
        } catch (Exception e) {
            ErrorController.raiseError("Cannot load ballroom view.");
            e.printStackTrace();
        }
    }

    @FXML
    public void openFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        File file = fileChooser.showOpenDialog(generate.getScene().getWindow());
        if (file != null) {
            InputFileParser parser = new InputFileParser();
            try {
                BallRoom ballRoom = parser.parse(new FileReader(file));
                GuestOutputGenerator generator = new GuestOutputGenerator();
                guest_input.setText(generator.generate(ballRoom.getGuests(), ""));

                if (ballRoom.getNewlywedsTable() != null) {
                    nw_table_type.setValue(ballRoom.getNewlywedsTable().getType());
                    nw_table_seats.getValueFactory().setValue(ballRoom.getNewlywedsTable().getSeats().length);

                    number_of_guest_tables.getValueFactory().setValue(ballRoom.getGuestsTables().length);

                    if (ballRoom.getGuestsTables().length > 0) {
                        TableType firstType = ballRoom.getGuestsTables()[0].getType();
                        Integer firstSeats = ballRoom.getGuestsTables()[0].getSeats().length;
                        boolean commonSettings = true;
                        for (Integer i = 0; i < individualSettings.getChildren().size(); i++) {
                            GridPane tableInfo = (GridPane) individualSettings.getChildren().get(i);
                            ComboBox tableType = (ComboBox) tableInfo.getChildren().get(1);
                            Spinner tableSeats = (Spinner) tableInfo.getChildren().get(2);

                            TableType type = ballRoom.getGuestsTables()[i].getType();
                            Integer seats = ballRoom.getGuestsTables()[i].getSeats().length;
                            tableType.setValue(type);
                            tableSeats.getValueFactory().setValue(seats);

                            if (!type.equals(firstType) || !seats.equals(firstSeats)) {
                                commonSettings = false;
                            }
                        }

                        if (commonSettings) {
                            guestsTablesCommonType.setValue(firstType);
                            guestsTablesCommonSeats.getValueFactory().setValue(firstSeats);
                            table_settings.selectToggle(common);
                        } else {
                            table_settings.selectToggle(individual);
                        }
                    }

                    showBallroom(ballRoom);
                }
            } catch (ParseException | IncompleteDataException | InvalidInputFormatException | InvalidDataException | InvalidOutputGenerationRequestException e) {
                ErrorController.raiseError(e.getMessage());
                e.printStackTrace();
            } catch (Exception e) {
                ErrorController.raiseError("Cannot load ballroom view.");
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void quit() {
        Platform.exit();
    }

    private void showBallroom(BallRoom ballRoom) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/ballroom.fxml"));
        BallroomController controller = new BallroomController(ballRoom);
        fxmlLoader.setController(controller);
        Parent page = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(page));
        stage.setTitle("Wedding Planner");
        stage.show();
    }

    private void setIndividualContent() {
        GridPane headers = new GridPane();
        AnchorPane.setTopAnchor(headers, 0.0);
        AnchorPane.setLeftAnchor(headers, 14.0);
        AnchorPane.setRightAnchor(headers, 14.0);
        headers.setMinHeight(42.0);
        headers.setPrefHeight(42.0);
        headers.setMaxHeight(42.0);
        headers.setAlignment(Pos.CENTER_LEFT);

        Label tableLabel = new Label("Table");
        tableLabel.setFont(Font.font("System", FontWeight.BOLD, FontPosture.ITALIC,21));
        tableLabel.setPadding(new Insets(0.0, 0.0, 0.0, 30.0));
        headers.add(tableLabel, 0, 0);

        Label typeLabel = new Label("Table Type");
        typeLabel.setFont(Font.font("System", FontWeight.BOLD, FontPosture.ITALIC,21));
        headers.add(typeLabel, 1, 0);

        Label seatsLabel = new Label("Number of seats");
        seatsLabel.setFont(Font.font("System", FontWeight.BOLD, FontPosture.ITALIC,21));
        headers.add(seatsLabel, 2, 0);

        for (int i = 1; i <= headers.getChildren().size(); i++) {
            ColumnConstraints column = new ColumnConstraints();
            column.setPercentWidth(100/headers.getChildren().size());
            headers.getColumnConstraints().add(column);
        }

        ScrollPane scrollPane = new ScrollPane();
        AnchorPane.setTopAnchor(scrollPane, 48.0);
        AnchorPane.setBottomAnchor(scrollPane, 0.0);
        AnchorPane.setLeftAnchor(scrollPane, 0.0);
        AnchorPane.setRightAnchor(scrollPane, 0.0);

        if (individualSettings == null) {
            updateIndividualSettings(0, number_of_guest_tables.getValue());
        }

        scrollPane.setContent(individualSettings);
        scrollPane.setFitToWidth(true);
        scrollPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);

        table_settings_pane.getChildren().clear();
        table_settings_pane.getChildren().add(headers);
        table_settings_pane.getChildren().add(scrollPane);
    }

    private GridPane createIndividualSettingsTableRow(Integer index) {
        GridPane tableRow = new GridPane();
        tableRow.setAlignment(Pos.CENTER);

        Label tableIndexLabel = new Label("Table" + String.valueOf(index));
        tableIndexLabel.setPadding(new Insets(0.0, 0.0, 0.0, 40.0));

        ComboBox<TableType> guestsTablesType = new ComboBox<>();
        setUpTableTypeCombobox(guestsTablesType);

        Spinner<Integer> guestsTablesSeats = new Spinner<>();
        setUpIntegerSpinner(guestsTablesSeats, 2, 100);

        tableRow.addRow(0, tableIndexLabel, guestsTablesType, guestsTablesSeats);

        for (int j = 1; j <= tableRow.getChildren().size(); j++) {
            ColumnConstraints column = new ColumnConstraints();
            column.setPercentWidth(100 / tableRow.getChildren().size());
            tableRow.getColumnConstraints().add(column);
        }

        return tableRow;
    }

    private void updateIndividualSettings(Integer oldRowNumber, Integer newRowNumber) {
        if (individualSettings == null) {
            individualSettings = new GridPane();
            individualSettings.setVgap(10);

            ColumnConstraints column = new ColumnConstraints();
            column.setPercentWidth(100);
            individualSettings.getColumnConstraints().add(column);
        }

        if (newRowNumber < oldRowNumber) {
            for (Integer i = oldRowNumber; i > newRowNumber; i--) {
                individualSettings.getChildren().remove(i-1);
            }
        } else {
            for (Integer i = oldRowNumber; i < newRowNumber; i++) {
                individualSettings.addRow(i, createIndividualSettingsTableRow(i+1));
            }
        }
    }

    private void setCommonContent() {
        GridPane commonSettings = new GridPane();
        commonSettings.setAlignment(Pos.CENTER);
        commonSettings.setHgap(10);
        commonSettings.setVgap(10);
        commonSettings.setPadding(new Insets(25.0, 0.0, 0.0, 0.0));

        Label tableTypeLabel = new Label("Table Type");
        tableTypeLabel.setFont(Font.font("System", FontWeight.BOLD, FontPosture.ITALIC,21));
        commonSettings.add(tableTypeLabel, 0, 0);

        if (guestsTablesCommonType == null) {
            guestsTablesCommonType = new ComboBox<>();
            setUpTableTypeCombobox(guestsTablesCommonType);
        }
        commonSettings.add(guestsTablesCommonType, 1, 0);

        Label numberOfSeatsLabel = new Label("Number of seats");
        numberOfSeatsLabel.setFont(Font.font("System", FontWeight.BOLD, FontPosture.ITALIC,21));
        commonSettings.add(numberOfSeatsLabel, 0, 1);

        if (guestsTablesCommonSeats == null) {
            guestsTablesCommonSeats = new Spinner<>();
            setUpIntegerSpinner(guestsTablesCommonSeats, 2, 100);
        }
        commonSettings.add(guestsTablesCommonSeats, 1, 1);

        AnchorPane.setTopAnchor(commonSettings, 0.0);
        AnchorPane.setLeftAnchor(commonSettings, 0.0);
        AnchorPane.setRightAnchor(commonSettings, 0.0);
        table_settings_pane.getChildren().clear();
        table_settings_pane.getChildren().add(commonSettings);
    }

    private void setUpTableTypeCombobox(ComboBox<TableType> combobox) {
        combobox.setItems(FXCollections.observableArrayList(TableType.values()));
        combobox.getSelectionModel().selectFirst();
        combobox.setConverter(new StringConverter<TableType>() {
            @Override
            public String toString(TableType object) {
                return object.name();
            }

            @Override
            public TableType fromString(String string) {
                return TableType.valueOf(string);
            }
        });
    }

    private void setUpIntegerSpinner(Spinner<Integer> spinner, Integer minValue, Integer maxValue) {
        NumberFormat format = NumberFormat.getIntegerInstance();
        TextFormatter<Integer> numberFormatter = new TextFormatter<>(new IntegerStringConverter(), minValue, c -> {
            if (c.isContentChange()) {
                ParsePosition parsePosition = new ParsePosition(0);
                format.parse(c.getControlNewText(), parsePosition);
                if (parsePosition.getIndex() == 0 ||
                        parsePosition.getIndex() < c.getControlNewText().length()) {
                    return null;
                }
            }
            return c;
        });
        spinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(minValue, maxValue));
        spinner.setEditable(true);
        spinner.getEditor().setTextFormatter(numberFormatter);
        spinner.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) return;
            if (!spinner.isEditable()) return;
            String text = spinner.getEditor().getText();
            SpinnerValueFactory<Integer> valueFactory = spinner.getValueFactory();
            if (valueFactory != null) {
                StringConverter<Integer> converter = valueFactory.getConverter();
                if (converter != null) {
                    Integer value = converter.fromString(text);
                    valueFactory.setValue(value);
                }
            }
        });
    }
}
