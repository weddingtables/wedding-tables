package org.weddingtables.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import org.weddingtables.model.ballroom.BallRoom;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestUnit;

/**
 * Created by Paweł on 2017-07-25.
 */
public class BallroomController {
    @FXML
    private ScrollPane guests;

    private BallRoom ballRoom;

    @FXML
    public void initialize() {
        VBox guestsList = new VBox();
        guests.setContent(guestsList);

        for (GuestUnit guestUnit : ballRoom.getGuests()) {
            if (guestUnit.getTable() == null) {
                for (Guest guest : guestUnit.getGuests()) {
                    Label guestLabel = new Label(guest.getFirstname() + " " + guest.getLastname());
                    guestLabel.setFont(Font.font("System", FontWeight.BOLD, FontPosture.REGULAR,16));
                    guestsList.getChildren().add(guestLabel);
                }
            }
        }
    }

    public BallroomController(BallRoom ballRoom) {
        this.ballRoom = ballRoom;
    }
}
