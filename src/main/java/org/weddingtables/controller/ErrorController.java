package org.weddingtables.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Created by Paweł on 2017-07-26.
 */
public class ErrorController {
    @FXML
    private Text description;

    @FXML
    public void onClick(ActionEvent event)
    {
        Stage current = (Stage) ((Node)event.getSource()).getScene().getWindow();
        current.close();
        event.consume();
    }

    private void setDescription(String description) {
        this.description.setText(description);
    }

    public static void raiseError(String desciption) {
        try {
            FXMLLoader loader = new FXMLLoader(ErrorController.class.getResource("/view/error.fxml"));
            Parent page = loader.load();
            ErrorController controller = loader.getController();
            controller.setDescription(desciption);
            Stage stage = new Stage();
            stage.setScene(new Scene(page));
            stage.setTitle("Wedding Planner");
            stage.setResizable(false);
            stage.sizeToScene();
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
