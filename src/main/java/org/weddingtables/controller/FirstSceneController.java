package org.weddingtables.controller;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class FirstSceneController {
    @FXML
    public void OnStartButton(ActionEvent event)
    {
        try {
            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/view/settings.fxml"));
            Parent page = loader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(page));
            stage.setTitle("Wedding Planner");
            stage.setResizable(false);
            stage.sizeToScene();

            Stage current = (Stage) ((Node)event.getSource()).getScene().getWindow();
            current.close();

            stage.show();
        } catch (Exception e) {
            ErrorController.raiseError("Cannot load settings view.");
            e.printStackTrace();
        }
        event.consume();
    }

    @FXML
    public void OnGiveUpButton()
    {
        Platform.exit();
    }
}
