package org.weddingtables.model.ballroom;

import org.weddingtables.exceptions.IncompleteDataException;
import org.weddingtables.exceptions.InvalidOutputGenerationRequestException;
import org.weddingtables.model.guest.GuestUnit;
import org.weddingtables.model.table.ITable;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Paweł on 2017-07-04.
 */
public class BallRoom {
    public final static String NEWLYWEDSTABLE = "newlywedsTable";
    public final static String GUESTSTABLES = "guestsTables";
    public final static String UNPLACEDGUESTS = "unplacedGuests";

    private final ITable newlywedsTable;
    private final ITable[] guestsTables;
    private final ArrayList<GuestUnit> guests;

    public BallRoom(ITable newlywedsTable, ITable[] guestsTables, ArrayList<GuestUnit> guests) throws IncompleteDataException {
        if (newlywedsTable == null ^ guestsTables == null) throw new IncompleteDataException(newlywedsTable == null ? "newly-weds table" : "guests tables");
        this.newlywedsTable = newlywedsTable;
        this.guestsTables = guestsTables;
        this.guests = guests;
    }

    public ITable getNewlywedsTable() {
        return newlywedsTable;
    }

    public ITable[] getGuestsTables() {
        return guestsTables;
    }

    public ArrayList<GuestUnit> getGuests() {
        return guests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BallRoom that = (BallRoom) o;

        if (getNewlywedsTable() != null ? !getNewlywedsTable().equals(that.getNewlywedsTable()) : that.getNewlywedsTable() != null)
            return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getGuestsTables(), that.getGuestsTables())) return false;
        return getGuests().equals(that.getGuests());
    }
}
