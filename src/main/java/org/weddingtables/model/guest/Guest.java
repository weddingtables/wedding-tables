package org.weddingtables.model.guest;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Paweł on 2017-07-01.
 */
public class Guest extends GuestUnit{
    private final String firstname;
    private final String lastname;

    public Guest(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    @Override
    public GuestUnitType getType() {
        return GuestUnitType.INDIVIDUAL;
    }

    @Override
    public ArrayList<Guest> getGuests() {
        ArrayList<Guest> guests = new ArrayList<>();
        guests.add(this);
        return guests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Guest guest = (Guest) o;

        if (!getFirstname().equals(guest.getFirstname())) return false;
        return getLastname().equals(guest.getLastname());
    }
}
