package org.weddingtables.model.guest;

import org.weddingtables.model.table.ITable;

import java.util.ArrayList;

/**
 * Created by Paweł on 2017-07-01.
 */
public abstract class GuestUnit {
    public final static String TYPE = "type";
    public final static String PLACE = "place";
    public final static String FIRSTNAME = "firstname";
    public final static String LASTNAME = "lastname";
    public final static String MEMBERS = "members";

    protected ITable table;
    protected GuestGroup group;

    public void setTable(ITable table) {
        this.table = table;
    }

    public void setGroup(GuestGroup group) {
        this.group = group;
    }

    public GuestGroup getGroup() {
        return group;
    }

    public ITable getTable() {
        return (group == null ? table : group.getTable());
    }

    public abstract GuestUnitType getType();

    public abstract ArrayList<Guest> getGuests();
}
