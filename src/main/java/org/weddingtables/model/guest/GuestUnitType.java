package org.weddingtables.model.guest;

/**
 * Created by Paweł on 2017-07-02.
 */
public enum GuestUnitType {
    INDIVIDUAL,
    GROUP
}
