package org.weddingtables.model.guest;

import org.weddingtables.exceptions.IncompleteDataException;
import org.weddingtables.model.table.ITable;

import java.util.ArrayList;

/**
 * Created by Paweł on 2017-07-02.
 */
public class GuestUnitBuilder {

    private final GuestUnitType type;
    private String firstname;
    private String lastname;
    private ArrayList<GuestUnit> members;

    public GuestUnitBuilder(GuestUnitType type) {
        this.type = type;

        if (type.equals(GuestUnitType.GROUP)) {
            this.members = new ArrayList<>();
        }
    }

    public GuestUnitBuilder setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public GuestUnitBuilder setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public GuestUnitBuilder addMember (GuestUnit member) {
        if (this.type.equals(GuestUnitType.GROUP)) {
            this.members.add(member);
        }
        return this;
    }

    public GuestUnit build() throws IncompleteDataException {
        GuestUnit guestUnit = null;

        switch (this.type) {
            case INDIVIDUAL:
                if (this.firstname == null) throw new IncompleteDataException(GuestUnit.FIRSTNAME);
                if (this.lastname == null) throw new IncompleteDataException(GuestUnit.LASTNAME);

                guestUnit = new Guest(this.firstname, this.lastname);
                break;
            case GROUP:
                if (this.members.isEmpty()) throw new IncompleteDataException(GuestUnit.MEMBERS);

                guestUnit = new GuestGroup(this.members);
                break;
        }

        return guestUnit;
    }
}
