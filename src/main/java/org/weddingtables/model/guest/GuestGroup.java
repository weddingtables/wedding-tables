package org.weddingtables.model.guest;

import java.util.ArrayList;

/**
 * Created by Paweł on 2017-07-01.
 */
public class GuestGroup extends GuestUnit {
    private final ArrayList<GuestUnit> members;

    public GuestGroup(ArrayList<GuestUnit> members) {
        this.members = members;
        for (GuestUnit member : this.members) {
            member.setGroup(this);
        }
    }

    public ArrayList<GuestUnit> getMembers() {
        return members;
    }

    @Override
    public GuestUnitType getType() {
        return GuestUnitType.GROUP;
    }

    @Override
    public ArrayList<Guest> getGuests() {
        ArrayList<Guest> guests = new ArrayList<>();
        for (GuestUnit member : this.members) {
            guests.addAll(member.getGuests());
        }
        return guests;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GuestGroup that = (GuestGroup) o;

        return getMembers().equals(that.getMembers());
    }
}
