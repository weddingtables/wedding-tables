package org.weddingtables.model.table;

import org.weddingtables.exceptions.InvalidDataException;

/**
 * Created by Paweł on 2017-07-02.
 */
public class TableFactory {

    public TableFactory() {}

    public ITable create(TableType type, Integer seats) throws InvalidDataException {
        ITable table = null;

        switch (type) {
            case ROUND:
                table = new RoundTable(seats);
                break;
            case RECTANGLE:
                table = new RectangleTable(seats);
                break;
            default:
                throw new InvalidDataException();
        }

        return table;
    }
}
