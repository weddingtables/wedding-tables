package org.weddingtables.model.table;

/**
 * Created by Paweł on 2017-07-01.
 */
public enum TableType {
    RECTANGLE,
    ROUND
}
