package org.weddingtables.model.table;

import org.weddingtables.exceptions.AlreadyOccupiedSeatException;
import org.weddingtables.exceptions.CannotRemoveGuestFromTableException;
import org.weddingtables.exceptions.InvalidDataException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestUnit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

/**
 * Created by Paweł on 2017-07-01.
 */
public abstract class Table implements ITable{
    public final static String TYPE = "type";
    public final static String SEATS = "seats";
    public final static String GUESTS = "guests";

    protected Guest[] seats;
    protected ArrayList<GuestUnit> guests;

    protected Table(Integer seats) throws InvalidDataException {
        if (seats < 1) throw new InvalidDataException();
        this.seats = new Guest[seats];
        this.guests = new ArrayList<>();
    }

    public abstract TableType getType();

    public Integer getFreeSeatsNumber() {
        Integer freeSeatsNumber = 0;
        for (Guest seat : seats) {
            if (seat == null) {
                freeSeatsNumber++;
            }
        }

        return freeSeatsNumber;
    }

    public void addGuest(GuestUnit guest) {
        while (guest.getGroup() != null) {
            guest = guest.getGroup();
        }
        this.guests.add(guest);
        guest.setTable(this);
    }

    public void removeGuest(GuestUnit guest) throws CannotRemoveGuestFromTableException {
        if (!this.guests.remove(guest)) throw new CannotRemoveGuestFromTableException();
        guest.setTable(null);
        for (Integer i = 0; i < seats.length; i++) {
            if (guest.getGuests().contains(seats[i])) {
                seats[i] = null;
            }
        }
    }

    public void placeGuest(Guest guest, Integer seat) throws AlreadyOccupiedSeatException, InvalidDataException {
        if (seat < 0 || seat >= seats.length) throw new InvalidDataException();
        if (seats[seat] != null) throw new AlreadyOccupiedSeatException();

        seats[seat] = guest;
    }

    public ArrayList<GuestUnit> getGuests() {
        return guests;
    }

    public Guest[] getSeats() { return seats; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Table)) return false;

        Table table = (Table) o;

        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getSeats(), table.getSeats())) return false;
        return getGuests().equals(table.getGuests());
    }
}
