package org.weddingtables.model.table;

import org.weddingtables.exceptions.InvalidDataException;

/**
 * Created by Paweł on 2017-07-01.
 */
public class RoundTable extends Table {

    public RoundTable(Integer seats) throws InvalidDataException {
        super(seats);
    }

    @Override
    public TableType getType() {
        return TableType.ROUND;
    }
}
