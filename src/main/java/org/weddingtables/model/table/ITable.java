package org.weddingtables.model.table;

import org.weddingtables.exceptions.AlreadyOccupiedSeatException;
import org.weddingtables.exceptions.CannotRemoveGuestFromTableException;
import org.weddingtables.exceptions.InvalidDataException;
import org.weddingtables.model.guest.Guest;
import org.weddingtables.model.guest.GuestUnit;

import java.util.ArrayList;

/**
 * Created by Paweł on 2017-07-01.
 */
public interface ITable {
    TableType getType();
    Integer getFreeSeatsNumber();
    void addGuest(GuestUnit guest);
    void removeGuest(GuestUnit guest) throws CannotRemoveGuestFromTableException;
    void placeGuest(Guest guest, Integer place) throws AlreadyOccupiedSeatException, InvalidDataException;
    ArrayList<GuestUnit> getGuests();
    Guest[] getSeats();
}
